package opentype

// http://web.archive.org/web/20240614172522/https://learn.microsoft.com/en-gb/typography/opentype/spec/maxp
// Version 0.5 (CFF only)
type MaxpVersion0p5Record struct {
	Version   int32
	NumGlyphs uint16
}

// Version 1.0
type MaxpRecord struct {
	MaxpVersion0p5Record
	MaxPoints, MaxContours                   uint16
	MaxCompositePoints, MaxCompositeContours uint16
	MaxZones                                 uint16
	MaxTwilightPoints                        uint16
	MaxStorage                               uint16
	MaxFunctionDefs, MaxInstructionDefs      uint16
	MaxStackElements, MaxSizeOfInstructions  uint16
	MaxComponentElements, MaxComponentDepth  uint16
}
