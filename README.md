# OpenType

Add to Go project with:

    go get gitlab.com/font8/opentype

In your Go code:

    r, err := os.Open("fontfile.ttf")
    font, err := opentype.OpenFont(r)

## Updating

If you have updated this library and want to use it in another
project (a tool), commit and push, and then:

    go get gitlab.com/font8/opentype@COMMIT-SHA

# END
