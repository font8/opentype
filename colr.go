package opentype

import (
	"encoding/binary"
	"log"
	"os"
	"path"
)

// https://learn.microsoft.com/en-us/typography/opentype/spec/colr#baseglyph-and-layer-records
type BaseGlyphRecord struct {
	GlyphID, FirstLayerIndex, NumLayers uint16
}
type LayerRecord struct {
	GlyphID, PaletteIndex uint16
}

// https://learn.microsoft.com/en-us/typography/opentype/spec/colr#colr-header
type COLRHeaderRecord struct {
	Version                uint16
	NumBaseGlyphRecords    uint16
	BaseGlyphRecordsOffset uint32
	LayerRecordsOffset     uint32
	NumLayerRecords        uint16
}

// Slightly higher level models

// Model for a ColrGlyph
type ColrGlyph struct {
	BaseGlyphIndex int
	Layers         []ColrLayer
}

type ColrLayer struct {
	GlyphIndex, Palette int
}

func COLRWrite(dir string, glyphs []ColrGlyph) {
	w, err := os.OpenFile(path.Join(dir, "COLR"),
		os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer w.Close()

	ls := []LayerRecord{}
	bs := []BaseGlyphRecord{}

	for _, glyph := range glyphs {
		li := uint16(len(ls))
		base := BaseGlyphRecord{GlyphID: uint16(glyph.BaseGlyphIndex),
			FirstLayerIndex: li,
			NumLayers:       uint16(len(glyph.Layers)),
		}
		bs = append(bs, base)
		for _, layer := range glyph.Layers {
			ls = append(ls,
				LayerRecord{uint16(layer.GlyphIndex), uint16(layer.Palette)})
		}

	}

	var o uint32

	baseGlyphsSize := binary.Size(bs)
	header := COLRHeaderRecord{}
	header.NumBaseGlyphRecords = uint16(len(bs))
	o += uint32(binary.Size(header))
	header.BaseGlyphRecordsOffset = o
	o += uint32(baseGlyphsSize)
	header.LayerRecordsOffset = o
	header.NumLayerRecords = uint16(len(ls))

	binary.Write(w, binary.BigEndian, header)
	binary.Write(w, binary.BigEndian, bs)
	binary.Write(w, binary.BigEndian, ls)
}
