package opentype

import (
	"math"
)

// Convert OpenType 16.16 Fixed point to floating point.
// This is exact.
func FixedToF64(s int32) float64 {
	return float64(s) / 0x10000
}

// Convert float to OpenType 16.16 Fixed point.
// This is not exact. But it will round-trip.
func F64ToFixed(f float64) int32 {
	return int32(math.RoundToEven(f * 0x10000))
}
