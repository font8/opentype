package opentype

// post table

import (
	"bytes"
	"encoding/binary"
	"log"
)

type PostHeaderRecord struct {
	Format                                uint32
	ItalicAngle                           int32
	UnderlinePosition, UnderlineThickness int16
	IsFixedPitch                          uint32
	MinMemType42, MaxMemType42            uint32
	MinMemType1, MaxMemType1              uint32
}

type Post struct {
	PostHeaderRecord
	Names []string
}

func PostOpen(bs []byte) *Post {
	post := &Post{}
	r := bytes.NewReader(bs)

	binary.Read(r, binary.BigEndian, &post.PostHeaderRecord)

	if post.Format == 0x20000 {
		var numGlyphs uint16
		binary.Read(r, binary.BigEndian, &numGlyphs)
		glyphNameIndex := make([]uint16, numGlyphs)
		binary.Read(r, binary.BigEndian, glyphNameIndex)

		names := []string{}

		for {
			// Read 1 byte for length of Pascal string
			l := make([]byte, 1)
			n, _ := r.Read(l)
			if n < len(l) {
				break
			}
			// Read pascal_string
			lps := int(l[0])
			pascal_string := make([]byte, lps)
			n, _ = r.Read(pascal_string)
			if n < lps {
				log.Fatal("Reached end of string in post table too early", bs)
			}
			names = append(names, string(pascal_string))
		}

		for _, name_index := range glyphNameIndex {
			post.Names = append(post.Names,
				PostGlyphName(names, int(name_index)))
		}
	}

	return post
}

func PostNames(post *Post) []string {
	return post.Names
}

// Return the glyph name for the given (glyph name) index.
// Standard names are returned for indexes 0 up to and including 257;
// The names slice is used for higher indexes.
func PostGlyphName(names []string, index int) string {
	if index > 257 {
		index -= 258
		return names[index]
	}

	return MacStandardGlyph[index]
}
