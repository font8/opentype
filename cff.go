package opentype

// Stuff to help with CFF (Compact Font Format)
// In this context CFF means a CFF embedded in an OTF container
// (in principle this could be separated, but no work has been
// done on that).

// CFF, and the parts modelled here, are mostly about structure
// and metadata.
// The bulk drawing data for glyphs is stored in Adobe Type 2
// Charstrings, and is handled by adobet2.go.

// REFERENCES
//
// [CFFSpec] (several URL options)
// https://adobe-type-tools.github.io/font-tech-notes/pdfs/5176.CFF.pdf
// https://wwwimages2.adobe.com/content/dam/acom/en/devnet/font/pdfs/5176.CFF.pdf

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"log"
	"strconv"
)

type CFF struct {
	B                  []byte
	Major, Minor       int
	OffSize            int
	Name, TopDictIndex CFFIndex
	GsubrIndex, String CFFIndex

	Gsubr      [][]byte
	Subr       [][][]byte
	TopMap     []CFFMap
	PrivateMap []CFFMap
}

// A simple go model for the DICT structures in a CFF.
type CFFMap map[string][]float64

func CFFOpen(bs []byte) *CFF {
	r := bytes.NewReader(bs)

	header := CFFHeader{}

	binary.Read(r, binary.BigEndian, &header)

	cff := &CFF{}
	cff.Major = int(header.Major)
	cff.Minor = int(header.Minor)
	cff.OffSize = int(header.OffSize)

	skip := 0
	o := int(header.HdrSize)
	cff.Name, skip = CFFIndexMake(bs[o:])
	o += skip

	cff.TopDictIndex, skip = CFFIndexMake(bs[o:])
	o += skip

	cff.String, skip = CFFIndexMake(bs[o:])
	o += skip

	cff.GsubrIndex, skip = CFFIndexMake(bs[o:])
	o += skip

	cff.Gsubr = cff.GsubrIndex.Item

	cff.B = bs

	cff.TopMaps()
	cff.Privatise()

	return cff
}

// Realise the TopDict items as maps in the cff struct.
func (cff *CFF) TopMaps() {
	cff.TopMap = make([]CFFMap, len(cff.TopDictIndex.Item))
	for i, bs := range cff.TopDictIndex.Item {
		items := CFFDictMake(bs)
		topmap := CFFDictAsMap(items)
		cff.TopMap[i] = topmap
	}
}

// Add the Private DICTs; and the Subrs which are contained
// with the Private block of bytes.
func (cff *CFF) Privatise() {
	cff.PrivateMap = make([]CFFMap, len(cff.TopMap))
	cff.Subr = make([][][]byte, len(cff.TopMap))
	for i, top := range cff.TopMap {
		v, ok := top["Private"]
		if !ok {
			log.Printf("Private DICT entry missing in Top DICT [%d]", i)
			continue
		}
		if len(v) != 2 {
			log.Printf("Private DICT value does not have 2 numbers in Top DICT [%d]: %v", i, v)
			continue
		}
		size := int(v[0])
		offset := int(v[1])
		bs := cff.B[offset : offset+size]
		items := CFFDictMake(bs)
		privatemap := CFFDictAsMap(items)
		cff.PrivateMap[i] = privatemap

		// Subrs
		s := privatemap["Subrs"]
		if len(s) != 1 {
			continue
		}
		// While the Subr offset is specified as being
		// relative to the beginning of the Private DICT data,
		// it turns out that its size is not
		// (necessarily?) included in the Private DICT
		// data size that was obtained from the Top DICT.
		soffset := int(v[0]) + offset
		index, _ := CFFIndexMake(cff.B[soffset:])
		cff.Subr[i] = index.Item
	}
}

// The GlyphCount is implied by many structures.
// Here we use the number of CharStrings.
func (cff *CFF) GlyphCount(top CFFMap) int {
	val := top["CharStrings"]
	if len(val) < 1 {
		log.Fatalf("CharStrings entry is missing in TopDict. %v", top)
	}
	offset := int(val[0])
	index, _ := CFFIndexMake(cff.B[offset:])
	return index.Length()
}

// All the charstrings for a CFF font. And their type.
func (cff *CFF) CharStrings(top CFFMap) ([][]byte, string, error) {
	val := top["CharStrings"]
	if len(val) < 1 {
		return nil, "", fmt.Errorf("CharStrings entry is missing in TopDict. %v", top)
	}

	charstringType := "CFF 2"
	v := top["CharstringType"]
	if len(v) >= 1 {
		charstringType = fmt.Sprintf("CFF %v", v[0])
	}

	offset := int(val[0])
	index, _ := CFFIndexMake(cff.B[offset:])

	return index.Item, charstringType, nil
}

// Return the charset strings, for font i.
// The result is a simple Go model which is a slice of strings,
// mapping from glyph index (starting at 0 for .notdef) to the glyph's name.
func (cff *CFF) CharsetStrings(i int) ([]string, error) {
	top := cff.TopMap[i]

	_, cid := top["ROS"]
	if cid {
		return nil, fmt.Errorf("CIDFont not supported")
	}

	n := cff.GlyphCount(top)
	sids := cff.CharsetSID(n, top)
	strings := cff.SIDToStrings(sids)
	return strings, nil
}

// Return the charset as a slice of SIDs (String IDs).
// Because a SID is mostly internal to a CFF font.
// most developers will want the more useful CharsetStrings() method.
func (cff *CFF) CharsetSID(glyphCount int, top CFFMap) []int {
	val := top["charset"]
	if len(val) < 1 {
		log.Fatal("charset entry is missing in TopDict.")
	}
	offset := int(val[0])
	charset, err := CFFCharsetMake(glyphCount, cff.B[offset:])
	if err != nil {
		log.Fatal("Can't handle charset format ", err)
	}
	return charset
}

// Convert SIDs to strings.
// The input sid is generally the output from CharsetSID.
func (cff *CFF) SIDToStrings(sid []int) []string {
	// Check that the internal list of CFF Standard Strings
	// matches the length of the same list in the spec.
	nstd := len(CFFStandardString)
	if 391 != nstd {
		log.Fatalf("Number of internal CFF Standard Strings [%d] deviates from 391, the number specified in The Compact Font Format Specification, Technical Note #5176, Version 1.0, Adobe, 2003-12-04")
	}

	res := make([]string, len(sid))
	for i := range sid {
		index := sid[i]
		if index < nstd {
			res[i] = CFFStandardString[index]
		} else {
			index -= nstd
			res[i] = string(cff.String.Item[index])
		}
	}
	return res
}

// Section 6 - Header
type CFFHeader struct {
	Major, Minor, HdrSize, OffSize uint8
}

// Section 5 - INDEX
type CFFIndexHeader struct {
	Count   uint16
	OffSize uint8
}
type CFFIndex struct {
	B      []byte
	Offset []int
	Data   []uint8
	Item   [][]byte
}

// Length of an index if the number of items in the index,
// which is one less than the length of the offset slice.
func (index *CFFIndex) Length() int {
	return len(index.Offset) - 1
}

func (index *CFFIndex) Itemise() {
	n := len(index.Offset) - 1
	index.Item = make([][]byte, n)
	for i := 0; i < n; i++ {
		p := index.Offset[i]
		q := index.Offset[i+1]
		index.Item[i] = index.Data[p:q]
	}
}

// Make a CFFIndex from its binary representation
// returning a model for the index, and its binary size
// (so that you can skip the index and proceed to next item).
func CFFIndexMake(bs []byte) (CFFIndex, int) {
	r := bytes.NewReader(bs)

	header := CFFIndexHeader{}
	binary.Read(r, binary.BigEndian, &header)

	index := CFFIndex{}
	index.B = bs
	index.Offset = make([]int, 1+header.Count)

	// Convert the offsets to a Go slice.
	// In the binary the offsets are 1,2,3, or 4 bytes,
	// according to header.OffSize.
	for i := range index.Offset {
		j := binary.Size(header) + i*int(header.OffSize)
		obs := bs[j : j+int(header.OffSize)]
		for len(obs) < 4 {
			obs = append([]byte{0}, obs...)
		}
		uoff := binary.BigEndian.Uint32(obs)
		index.Offset[i] = int(uoff)
	}

	dataOffset := binary.Size(header) +
		len(index.Offset)*int(header.OffSize) - 1
	index.Data = bs[dataOffset:]

	size := index.Offset[len(index.Offset)-1] + dataOffset

	// fill in the Item slice
	index.Itemise()

	return index, size
}

type CFFDictItem struct {
	// many of these are integeres, they are converted to float64 type
	Op   []float64
	Code string
}

// Create a map from a slice of CFFDictItem.
// If there are any duplicate keys then the construction of the
// map overwrites them, meaning only the last of the duplicate
// keys will be used.
// Duplicate keys shouldn't happen in real-world fonts, but
// the alist-list structure doesn't prohibit it.
// The map's values share the slices ([]float64) with the input.
func CFFDictAsMap(d []CFFDictItem) CFFMap {
	m := map[string][]float64{}
	for _, item := range d {
		m[item.Code] = item.Op
	}
	return m
}

// Convert the binary representation of a CFF DICT
// into a slice of modelled items (value, key pairs).
// Originally this only handled operators in TopDict, but
// now handles both Top and Private DICT operators.
// The spec makes it unclear what operators are allowed in
// what dictionaries.
func CFFDictMake(bs []byte) []CFFDictItem {
	// A key is associated with a value.
	// A key (called _operator_ in the spec),
	// is a number identified with a symbolic string.
	// A value is a short sequence of operands (often 1);
	// each operand is a number.
	// An operand can be a 1-, 2-, 4-byte integer or a
	// (packed) decimal floating point number.
	// In this Go model, each number is modelling as a
	// float64 regardless of its original representation
	// (this accurately models all the integers, but
	// rounds many of the packed decimal floats).

	// Parsing a CFF Dict works like a crude stack machine.
	// There is a stack of numbers that forms the incomplete value.
	// the byte stream is processed byte-by-byte, with
	// the next byte determing whether we have
	// an operand, which is pushed onto the stack, or
	// an operator, which receives all the operands on the stack
	// and empties the stack.
	items := []CFFDictItem{}
	stack := []float64{}

	pop := func(code string) {
		items = append(items, CFFDictItem{stack, code})
		stack = []float64{}
	}

	// consume 1 byte, pop entire stack, and append item.
	pop1 := func(code string) {
		bs = bs[1:]
		pop(code)
	}

	for len(bs) > 0 {
		b := bs[0]
		switch {
		// [CFFSpec] Section 9 - Top DICT Data
		// 0 to 21 specify operators.
		// See also Section 15 - Private DICT Data
		// The following operator ranges are in Private DICT:
		// - 6 to 11
		case 0 <= b && b <= 11:
			code := []string{
				"version",
				"Notice",
				"FullName",
				"FamilyName",
				"Weight",
				"FontBBox",
				"BlueValues",
				"OtherBlues",
				"FamilyBlues",
				"FamilyOtherBlues",
				"StdHW",
				"StdVW",
			}[b]
			pop1(code)

		case 12 == b:
			// extended codes
			bs = bs[1:]
			b := bs[0]
			switch b {
			case 0:
				pop1("Copyright")
			case 3:
				pop1("UnderlinePosition")
			case 4:
				pop1("UnderlineThickness")
			case 7:
				pop1("FontMatrix")
			// 12 codes 9 to 19 are in Private DICT
			case 9:
				pop1("BlueScale")
			case 10:
				pop1("BlueShift")
			case 11:
				pop1("BlueFuzz")
			case 12:
				pop1("StempSnapH")
			case 13:
				pop1("StempSnapV")
			case 14:
				pop1("ForceBold")

			case 17:
				pop1("LanguageGroup")
			case 18:
				pop1("ExpansionFactor")
			case 19:
				pop1("initialRandomSeed")

			// 12 codes 30 to 36 are used in CIDFonts
			case 30:
				pop1("ROS")
			case 31:
				pop1("CIDFontVersion")
			case 32:
				pop1("CIDFontRevision")
			case 33:
				pop1("CIDFontType")
			case 34:
				pop1("CIDCount")
			case 35:
				pop1("UIDBase")
			case 36:
				pop1("FDArray")
			case 37:
				pop1("FDSelect")
			case 38:
				pop1("FontName")

			default:
				code := fmt.Sprintf("Unknown 12 code %d", b)
				pop(code)
				bs = bs[1:]
			}

		case 13 == b:
			pop1("UniqueID")
		case 14 == b:
			pop1("XUID")
		case 15 == b:
			pop1("charset")
		case 16 == b:
			pop1("Encoding")
		case 17 == b:
			pop1("CharStrings")
		case 18 == b:
			pop1("Private")

			// 19 to 21 are in Private DICT
		case 19 == b:
			pop1("Subrs")
		case 20 == b:
			pop1("defaultWidthX")
		case 21 == b:
			pop1("nominalWidthX")

			// operands, see Section 4 - DICT Data.
		case 28 == b:
			// 2 byte uint
			v := int(int16(binary.BigEndian.Uint16(bs[1:3])))
			stack = append(stack, float64(v))
			bs = bs[3:]
			break
		case 29 == b:
			// 4 byte uint
			v := int(int32(binary.BigEndian.Uint32(bs[1:5])))
			stack = append(stack, float64(v))
			bs = bs[5:]
		case 30 == b:
			// a decimal float
			s := ""
			bs = bs[1:]
			// The nibble buffer. Up to 2 nibbles.
			// When needed, the next byte is
			// consumed and converted to 2 nibbles.
			nibble := []byte{}
		flotoken:
			for {
				if len(nibble) == 0 {
					// Consume a byte and convert to nibbles.
					nibble = []byte{bs[0] >> 4, bs[0] & 0xf}
					bs = bs[1:]
				}
				// Consume a nibble
				n := nibble[0]
				nibble = nibble[1:]
				switch n {
				case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9:
					s += fmt.Sprint(n)
				case 0xa:
					s += "."
				case 0xb:
					s += "E"
				case 0xc:
					s += "E-"
				case 0xd:
					log.Fatalf("nibble 0xd encountered.  Tokenised so far: %v", s)
				case 0xe:
					s += "-"
				case 0xf:
					break flotoken
				}
			}
			f, err := strconv.ParseFloat(s, 64)
			if err != nil {
				log.Fatalf("Problem parsing floating in dict operand %s. %v", s, err)
			}
			stack = append(stack, f)
			break
		case 32 <= b && b <= 246:
			v := int(b) - 139
			stack = append(stack, float64(v))
			bs = bs[1:]
			break
		case 247 <= b && b <= 250:
			v := (int(b)-247)*256 + int(bs[1]) + 108
			stack = append(stack, float64(v))
			bs = bs[2:]
			break
		case 251 <= b && b <= 254:
			v := (251-int(b))*256 - int(bs[1]) - 108
			stack = append(stack, float64(v))
			bs = bs[2:]
			break

			// reserved (22 to 27, 31, 255) and unimplemented.
		default:
			log.Fatal("Unknown code ", b, bs)
		}
	}
	if len(stack) > 0 {
		items = append(items, CFFDictItem{stack,
			"Missing Operator"})
	}
	return items
}

type SIDRange struct {
	SID, N int
}

// Section 13 - Charsets
// Low level primitive.
// Convert the bytes that represent the charset to
// a slice of n SIDs (String IDs, each one modelled as an int).
// The first SID is always 0 (for .notdef).
// This function prepends this 0, but
// it never appears in the representation.
// The output slice will be length n (or there is an error).
func CFFCharsetMake(n int, bs []byte) ([]int, error) {
	format := bs[0]

	switch format {
	case 0:
		// an array
		bs = bs[1:]
		buf := bytes.NewReader(bs)
		uints := make([]uint16, n-1)
		err := binary.Read(buf, binary.BigEndian, uints)
		if err != nil {
			return nil, err
		}

		sids := []int{0}
		for _, s := range uints {
			sids = append(sids, int(s))
		}
		return sids, nil

	case 1:
		rs := []SIDRange{}
		i := 0
		bs = bs[1:]
		// n is the number of glyphs, but
		// the first glyph is always .notdef and
		// is not represented in the charset.
		// Therefore the loop limit condition is n-1.
		for i < n-1 {
			sid := int(binary.BigEndian.Uint16(bs[0:2]))
			l := int(bs[2])
			rs = append(rs, SIDRange{sid, l})
			bs = bs[3:]
			i += 1 + l
		}
		return SIDRangeFlatten(rs), nil
	case 2:
		// Same as case 1, but with 2 bytes for l (range length).
		rs := []SIDRange{}
		i := 0
		bs = bs[1:]
		// n is the number of glyphs, but
		// the first glyph is always .notdef and
		// is not represented in the charset.
		// Therefore the loop limit condition is n-1.
		for i < n-1 {
			sid := int(binary.BigEndian.Uint16(bs[0:2]))
			l := int(binary.BigEndian.Uint16(bs[2:4]))
			rs = append(rs, SIDRange{sid, l})
			bs = bs[4:]
			i += 1 + l
		}
		return SIDRangeFlatten(rs), nil
	default:
		return nil, fmt.Errorf("Charset Format %d %v", format, bs[1:5])
	}
}

// Convert a slice of SIDRange structs to a flat slice of SIDs.
func SIDRangeFlatten(ranges []SIDRange) []int {
	sids := []int{0}
	for _, r := range ranges {
		sid := r.SID
		for i := 0; i <= r.N; i++ {
			sids = append(sids, sid+i)
		}
	}
	return sids
}
