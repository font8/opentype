package opentype

// OS/2 table

// According to Microsoft documentation, Versions 2, 3, and 4
// have the same structure (but possibly slightly different interpretations).
// https://learn.microsoft.com/en-gb/typography/opentype/spec/os2#version-4
type OS2HeaderVersion2Record struct {
	OS2HeaderVersion1Record
	XHeight, CapHeight int16
	DefaultChar        uint16
	BreakChar          uint16
	MaxContext         uint16
}

// https://learn.microsoft.com/en-gb/typography/opentype/spec/os2#version-1
type OS2HeaderVersion1Record struct {
	OS2HeaderVersion0Record
	CodePageRange [2]uint32 // Only in Version ≥ 1
}

// https://learn.microsoft.com/en-gb/typography/opentype/spec/os2#version-0
type OS2HeaderVersion0Record struct {
	Version                                  uint16 // 1
	XAvgCharWidth                            int16
	WeightClass, WidthClass                  uint16
	FsType                                   uint16
	YSubscriptXSize, YSubscriptYSize         int16
	YSubscriptXOffset, YSubscriptYOffset     int16
	YSuperscriptXSize, YSuperscriptYSize     int16
	YSuperscriptXOffset, YSuperscriptYOffset int16
	YStrikeoutSize, YStrikeoutPosition       int16
	FamilyClass                              int16
	Panose                                   [10]byte
	UnicodeRange                             [4]uint32
	VendID                                   [4]byte
	FsSelection                              uint16
	FirstCharIndex, LastCharIndex            uint16
	TypoAscender, TypoDescender, TypoLineGap int16
	WinAscent, WinDescent                    uint16
}
