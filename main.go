package opentype

// Package for handling OpenType Font Files.
// Should work with TTF too.

// On Linux for fonts check /usr/share/fonts/truetype/dejavu/
// and similar places.
// The command `fc-list` is also useful too.

// REFERENCES

// [CFF2003] The Compact Font Format Specification,
//   Technical Note #5176, Version 1.0, 4 December 2003.

// [OTSPEC] OpenType Specification,
//   https://docs.microsoft.com/en-us/typography/opentype/spec/

// [TTRM] TrueType Reference Manual,
//   https://developer.apple.com/fonts/TrueType-Reference-Manual/

import (
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"fmt"
	"gitlab.com/font8/coff"
	"io"
	"log"
)

// Each table in a, correctly formed, OTF file is
// modelled with a tag and its content as a slice of bytes.
type Table struct {
	Tag      string
	Checksum uint32
	Offset   uint32
	B        []byte
}

// Number of Glyphs
func (font *T) GlyphCount() (int, error) {
	blocks, err := font.GlyphDataBlocks()
	if err != nil {
		return 0, err
	}
	return len(blocks), nil
}

// Return Glyph from its index.
func (font *T) FetchGlyph(i int) OutlineGlyph {
	switch font.GlyphType {
	case "":
		log.Fatal("GlyphCount() must be called first")
	case "glyf":
		return &TTGlyph{font, i}
	case "CFF 2":
		return &CFF2Glyph{font, 0, i}
	default:
		log.Fatalf("UNEXPECTED GlyphType %c", font.GlyphType)
	}
	return nil
}

// Implements the seefont glyph subcommand.
func Glyph(font *T) error {
	blocks, err := font.GlyphDataBlocks()
	if err != nil {
		return err
	}
	for i, block := range blocks {
		header := GlyfHeaderRecord{}
		binary.Read(bytes.NewBuffer(block),
			binary.BigEndian, &header)
		fmt.Printf("%d %d [%d %d] [%d %d] %s\n",
			i, header.NumberOfContours,
			header.XMin, header.YMin,
			header.XMax, header.YMax,
			hex.EncodeToString(block))
	}
	return nil
}

func (font *T) GlyphDataBlocks() ([][]byte, error) {
	if font.GlyphData == nil {
		err := SetGlyphData(font)
		if err != nil {
			return nil, err
		}

	}
	return font.GlyphData, nil
}

// Set the font.GlyphData field.
func SetGlyphData(font *T) error {
	_, ok := font.Table["head"]
	if !ok {
		return errors.New("Missing `head` table")
	}

	_, ok = font.Table["loca"]
	if ok {
		data, err := DataBlocksFromGlyf(font)
		if err != nil {
			return err
		}
		font.GlyphData = data
		font.GlyphType = "glyf"
		return nil
	}

	_, ok = font.Table["CFF "]
	if ok {
		err := SetFromCFF(font)
		if err != nil {
			return err
		}
		return nil
	}

	return errors.New("No `loca`, no `CFF`; no glyph data found")
}

// Produce the slice of glyph data blocks from the `glyf` table.
func DataBlocksFromGlyf(font *T) ([][]byte, error) {
	head, ok := font.Table["head"]
	if !ok {
		log.Fatal("Missing `head` table; do not call unless `head` is present")
	}
	loca, ok := font.Table["loca"]
	if !ok {
		log.Fatal("Missing `loca` table; do not call unless `loca` is present")
	}

	longLoc := 0 != binary.BigEndian.Uint16(head.B[50:52])

	glyf, ok := font.Table["glyf"]
	if !ok {
		return nil, errors.New("Missing `glyf` table")
	}
	locations := Loca(&loca, longLoc)

	blocks := [][]byte{}

	p := 0
	i := 0
	for o := range locations {
		if i > 0 {
			b := glyf.B[p:o]
			blocks = append(blocks, b)
		}
		p = o
		i++
	}

	return blocks, nil
}

// Set data and metadata from CFF
func SetFromCFF(font *T) error {
	// There can be many fonts in a CFF.
	// This just picks the first one.
	fontIndex := 0
	table, ok := font.Table["CFF "]
	if !ok {
		log.Fatal("Missing `CFF ` table; do not call unless `CFF ` is present")
	}
	font.cff = CFFOpen(table.B)
	top := font.cff.TopMap[fontIndex]
	// Subr in private dict; Gsubr after

	// Quoting from Section 16 of [CFF2003]
	// Local subrs are stored in an INDEX structure which
	// is located via the offset operand of the Subrs operator
	// in the Private DICT. A font without local subrs has
	// no Subrs operator in the Private DICT.
	// [...]
	// Global subrs are stored in an INDEX structure which
	// follows the String INDEX. A FontSet without any global subrs is
	// represented by an empty Global Subrs INDEX.

	data, ty, err := font.cff.CharStrings(top)
	if err != nil {
		return err
	}
	font.GlyphData = data
	font.GlyphType = ty
	return nil
}

// Record (on disk format) for a single glyph in the glyf table.
// https://docs.microsoft.com/en-gb/typography/opentype/spec/glyf#glyph-headers
type GlyfHeaderRecord struct {
	NumberOfContours int16
	XMin, YMin       int16
	XMax, YMax       int16
}

// https://docs.microsoft.com/en-gb/typography/opentype/spec/head
type HeadRecord struct {
	Version                         uint32
	FontRevision                    int32
	ChecksumAdjustment, MagicNumber uint32
	Flags                           uint16
	UnitsPerEm                      uint16
	Created, Modified               uint64
	XMin, YMin, XMax, YMax          int16
	MacStyle                        uint16
	LowestRecPPEM                   uint16
	FontDirectionHint               uint16
	IndexToLocFormat                int16
	GlyphDataFormat                 int16
}

// Return a (chan yielding the) sequence of
// offsets into the glyf table.
// Offsets are byte offsets regardless of longFormat
// (shorts are converted).
func Loca(loca *Table, long bool) <-chan int {
	b := loca.B
	ch := make(chan int)
	if long {
		go func() {
			for i := 0; i < len(b); i += 4 {
				ch <- int(binary.BigEndian.Uint32(b[i : i+4]))
			}
			close(ch)
		}()
	} else {
		go func() {
			for i := 0; i < len(b); i += 2 {
				ch <- 2 * int(binary.BigEndian.Uint16(b[i:i+2]))
			}
			close(ch)
		}()
	}
	return ch
}

// Global header tables
type OffsetTableRecord struct {
	SfntVersion                            uint32
	NumTables                              uint16
	SearchRange, EntrySelector, RangeShift uint16
}

type T struct {
	OffsetTableRecord
	Table     map[string]Table
	Tags      []string // Just the tags, in the order they appear
	GlyphData [][]byte
	GlyphType string // "glyf" or "CFF 2"
	Source    string // intended to be used as a file name
	cff       *CFF   // only if a CFF font
}

// Return the flavor of the font as a string.
// Flavor is a term used by WOFF to refer to the 4-byte
// signature of the contained sfnt font-file.
// Apple TrueType uses "scaler type";
// Microsoft OpenType uses "sfntVersion".
// The input is the uint32 made up of the first 4 bytes of the file.
// If all 4 bytes are ASCII then they are returned as a 4 byte string.
// Otherwise a hex representation of the 32-bit value is returned.
func (font *T) Flavor() string {
	u := font.SfntVersion
	s := ""
	for i := 0; i < 4; i++ {
		b := ((u << (8 * i)) & 0xff000000) >> 24
		if 0x20 <= b && b < 0x7f {
			s += string(rune(b))
		} else {
			return fmt.Sprintf("%#08x", u)
		}
	}
	return s
}

type OutlineGlyph interface {
	ToOutline() *coff.Outline
}

func NewT() *T {
	result := &T{}
	result.Table = map[string]Table{}
	return result
}

func OpenFont(r io.ReadSeeker) (*T, error) {
	font := NewT()
	err := binary.Read(r, binary.BigEndian, &font.OffsetTableRecord)
	if err != nil {
		return nil, err
	}

	numTables := int(font.NumTables)
	tables, err := Tables(r, numTables)
	if err != nil {
		return nil, err
	}
	for _, table := range tables {
		if _, ok := font.Table[table.Tag]; ok {
			log.Printf("Table `%s` is repeated.\n", table.Tag)
		}
		font.Table[table.Tag] = table
		font.Tags = append(font.Tags, table.Tag)
	}
	return font, nil
}

// Return the glyph names, in glyph index order
// so that the slice can be indexed by glyph number
// to get the correct name.
// Uses either the CFF or the post table.
func (font *T) GlyphNames() ([]string, error) {
	cff, ok := font.Table["CFF "]
	if ok && len(cff.B) > 0 {
		return font.CFFGlyphNames(&cff)
	}
	post := font.Table["post"]
	return font.PostGlyphNames(&post), nil
}

func (font *T) CFFGlyphNames(table *Table) ([]string, error) {
	cff := CFFOpen(table.B)
	glyphNames, err := cff.CharsetStrings(0)
	return glyphNames, err
}

func (font *T) PostGlyphNames(table *Table) []string {
	post := PostOpen(table.B)
	return PostNames(post)
}

type TableRecord struct {
	Tag      [4]byte
	Checksum uint32
	Offset   uint32
	Length   uint32
}

// https://docs.microsoft.com/en-gb/typography/opentype/spec/otff#organization-of-an-opentype-font
func Tables(r io.ReadSeeker, numTables int) ([]Table, error) {
	records := make([]TableRecord, numTables)
	err := binary.Read(r, binary.BigEndian, records)
	if err != nil {
		if errors.Is(err, io.EOF) {
			return nil, fmt.Errorf("EOF (%w) reading table directory (not an OpenType file?); v tables", err, numTables)
		}
		return nil, err
	}

	tables := []Table{}
	for i := 0; i < numTables; i++ {
		t := &records[i]
		_, err := r.Seek(int64(t.Offset), io.SeekStart)
		if err != nil {
			return nil, fmt.Errorf("Couldn't seek to table %v (not an OpenType file?): %w", err)
		}
		buf := make([]byte, t.Length)
		_, err = r.Read(buf)
		if err != nil {
			return nil, err
		}
		tables = append(tables,
			Table{string(t.Tag[:]),
				t.Checksum, t.Offset, buf})
	}
	return tables, nil
}
