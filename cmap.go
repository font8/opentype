package opentype

// cmap table

// Reading and writing (compiling) cmap table.
// Only subtables in format 4 and 12 are implemented.

// Some confusion is evident.
// For writing, the map is modelled as the Go type:
// map[rune]int
// For reading, the segments and items are exposed.

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"sort"
	"unicode"
)

type CmapHeaderRecord struct {
	Version, NumTables uint16
}

// This is the record for the triple that encodes each subtable.
// In OpenType terminology (which is shorter and better) this is
// called an EncodingRecord.
// TrueType has the same record, but calls it subtable, and
// the EncodingID slot is called PlatformSpecificID.
// https://developer.apple.com/fonts/TrueType-Reference-Manual/RM06/Chap6cmap.html
type EncodingRecord struct {
	PlatformID uint16
	EncodingID uint16
	Offset     uint32
}

// https://docs.microsoft.com/en-gb/typography/opentype/spec/cmap#format-4-segment-mapping-to-delta-values
type Format4HeaderRecord struct {
	Format, Length, Language                           uint16
	SegCountX2, SearchRange, EntrySelector, RangeShift uint16
}

type Cmap struct {
	Table
	Subtable []EncodingRecord
	Offset   []int
}

// A simple Go model for a single item of a cmap subtable;
// it maps a unicode codepoint to glyph index.
type CmapItem struct {
	Code  rune
	Glyph int
}

// Shared interface used by format 4 and 12
type Segment interface {
	String() string
	Codes() []rune
	Items() []CmapItem
}

func NewCmap(t Table) *Cmap {
	cmap := Cmap{Table: t}
	cmap.readSubtables()

	return &cmap
}

func (cmap *Cmap) Subtables() []EncodingRecord {
	if cmap.Subtable == nil {
		log.Fatal("subtable not initialised")
	}
	return cmap.Subtable
}

func (cmap *Cmap) readSubtables() {
	r := bytes.NewReader(cmap.B)

	header := &CmapHeaderRecord{}

	binary.Read(r, binary.BigEndian, header)
	n := int(header.NumTables)

	sub := make([]EncodingRecord, n)

	binary.Read(r, binary.BigEndian, sub)

	cmap.Subtable = sub

	// Create a set offsets
	// (later sorted).
	cmap_offsets := make(map[int]struct{})
	for i := 0; i < n; i++ {
		cmap_offsets[int(sub[i].Offset)] = struct{}{}
	}

	cmap_offset := []int{}
	for o, _ := range cmap_offsets {
		cmap_offset = append(cmap_offset, o)
	}
	sort.IntSlice(cmap_offset).Sort()
	cmap.Offset = cmap_offset
}

// Return a string starting with 2 numbers separated by /:
// PLAT/END (eg 0/3 for Unicode BMP);
// if the platform/encoding combination is known a short
// description is appended to the returned string.
func (sub *EncodingRecord) Platform() string {
	s := fmt.Sprintf("%d/%d", sub.PlatformID, sub.EncodingID)
	switch sub.PlatformID {
	case 0:
		s += " Unicode"
		switch sub.EncodingID {
		case 0:
			return s + "/Default"
		case 1:
			return s + "/1.1"
		case 2:
			return s + "/ISO-10646:1993"
		case 3:
			return s + "/BMP"
		case 4:
			return s + "/BMP+non-BMP"
		case 5:
			return s + "/Variation"
		case 6:
			return s + "/Full"
		}
	case 1:
		s += " Macintosh"
		switch sub.EncodingID {
		// These are Macintosh Quickdraw script manager codes.
		// See https://developer.apple.com/fonts/TrueType-Reference-Manual/RM06/Chap6name.html
		case 0:
			s += "/Roman" // EG Compagnon
		case 3:
			s += "/Korean" // EG IBMPlexSansKR-Medium
		}
	case 3:
		s += " Microsoft"
		switch sub.EncodingID {
		case 1:
			s += "/Unicode-BMP"
		case 10:
			s += "/Unicode-UCS-4" // EG Ouroboros
		}
	}
	return s
}

// Models a segment of a cmap to glyph mapping
// https://docs.microsoft.com/en-us/typography/opentype/spec/cmap#format-4-segment-mapping-to-delta-values
type Cmap4Segment struct {
	StartCode, EndCode uint16
	Delta              int16
	Offset             uint16
	// when Offset is 0 this is nil;
	// otherwise (Offset it not 0), this is the relevant
	// slice into the glyphIndexArray, such that the StartCode
	// corresponds to the first element of this slice (but
	// don't forget to add Delta).
	// It should be read from the file when this Segmnent
	// model is constructed.
	GlyphIndex []uint16
}

// Convert to string representation.
func (seg *Cmap4Segment) String() string {
	from := ""
	to := ""

	seg_len := seg.EndCode - seg.StartCode + 1
	if seg_len == 1 {
		from = fmt.Sprintf("[%U]", seg.StartCode)
	} else {
		from = fmt.Sprintf("[%U-%U]", seg.StartCode, seg.EndCode)
	}

	if seg.Offset == 0 {
		start_glyph := seg.StartCode + uint16(seg.Delta)
		if seg_len == 1 {
			to = fmt.Sprintf("%d", start_glyph)
		} else {
			to = fmt.Sprintf("%d-%d", start_glyph, start_glyph+seg_len-1)
		}
		return from + " -> " + to
	} else {
		return fmt.Sprintf("%s -> (Delta %v) Table %v",
			from, seg.Delta, seg.GlyphIndex)
	}
}

// Convert to list of codes (the left-hand side of the map).
func (seg *Cmap4Segment) Codes() []rune {
	if seg.EndCode == 0xFFFF {
		return nil
	}
	rs := []rune{}
	for i := seg.StartCode; i <= seg.EndCode; i += 1 {
		rs = append(rs, rune(i))
	}
	return rs
}

// Convert to list of items (one key,value pair per entry).
// For details of segment entries, see
// https://web.archive.org/web/20240627153923/https://developer.apple.com/fonts/TrueType-Reference-Manual/RM06/Chap6cmap.html
func (seg *Cmap4Segment) Items() []CmapItem {
	if seg.EndCode == 0xFFFF {
		return nil
	}
	items := []CmapItem{}

	udelta := uint16(seg.Delta)

	for i := seg.StartCode; i <= seg.EndCode; i += 1 {
		glyph := -1
		if seg.Offset == 0 {
			glyph = int(i + udelta)
		} else {
			char_code_offset := i - seg.StartCode
			glyph_index_biased := seg.GlyphIndex[char_code_offset]
			if udelta != 0 {
				log.Fatalf("cmap format 4 segment has Delta != 0; which i haven't tested.\n%v\n", seg)
			}
			glyph = int(glyph_index_biased + udelta)
		}
		item := CmapItem{rune(i), glyph}
		items = append(items, item)
	}
	return items
}

// Return the format of a cmap subtable as a int;
// a negative number is returned if the format is not an integer
// (format 12 and some others have a slot for fractional
// sub-format, but in 2023 it is only ever 0).
// Requires its offset (obtained from cmap.Offset).
func (cmap *Cmap) SubtableFormat(offset int) int {
	b := cmap.B[offset:]
	format := int(binary.BigEndian.Uint16(b[:2]))
	switch format {
	case 8, 10, 12, 13:
		minor := binary.BigEndian.Uint16(b[2:4])
		if minor != 0 {
			return -(format<<16 | int(minor))
		}
	}
	return format
}

// Return the segments of the subtable at the given offset.
// Only for formats 4 and 12.0.
func (cmap *Cmap) Segments(offset int) []Segment {
	b := cmap.B[offset:]

	switch cmap.SubtableFormat(offset) {
	case 4:
		return Format4Segments(b)
	case 12:
		return Format12Segments(b)
	}
	return nil
}

// Return a unicode.RangeTable that covers the char code
// ranges of the segments.
func (cmap *Cmap) RangeTable(offset int) *unicode.RangeTable {
	b := cmap.B[offset:]

	switch cmap.SubtableFormat(offset) {
	case 4:
		rt := &unicode.RangeTable{}
		ss := Format4Segments(b)
		for _, stringer := range ss {
			segment := stringer.(*Cmap4Segment)
			start_glyph := segment.StartCode + uint16(segment.Delta)
			if start_glyph == 0 {
				continue
			}
			r16 := unicode.Range16{
				segment.StartCode, segment.EndCode, 1}
			rt.R16 = append(rt.R16, r16)
		}
		return rt
	}
	return nil
}

// https://docs.microsoft.com/en-gb/typography/opentype/spec/cmap#format-4-segment-mapping-to-delta-values
func Format4Segments(b []byte) []Segment {
	// https://docs.microsoft.com/en-gb/typography/opentype/spec/cmap#format-4-segment-mapping-to-delta-values

	header := &Format4HeaderRecord{}

	// Read header, followed by N each
	// of endCode startCode delta;
	// with, for good measure, 2 bytes of padding
	// between the endCode array and the startCode array.
	// (where N is segCount, the number of segments)
	r := bytes.NewReader(b)
	binary.Read(r, binary.BigEndian, header)
	segCount := int(header.SegCountX2 / 2)
	endCode := make([]uint16, segCount)
	startCode := make([]uint16, segCount)
	delta := make([]int16, segCount)
	offset := make([]uint16, segCount)

	binary.Read(r, binary.BigEndian, endCode)
	var padding uint16
	binary.Read(r, binary.BigEndian, &padding)
	binary.Read(r, binary.BigEndian, startCode)
	binary.Read(r, binary.BigEndian, delta)

	rangeOffsetBase, err := r.Seek(0, io.SeekCurrent)
	if err != nil {
		log.Fatal(err)
	}
	binary.Read(r, binary.BigEndian, offset)

	// Be warned: the computation of the offset is...
	// charmingly baroque.

	segments := []Segment{}
	for i := 0; i < segCount; i++ {
		var glyphIndex []uint16
		// A handful of mostly old fonts appear to have
		// an incorrect sentinel segment
		// in which startCode=endCode=0xFFFF (as expected)
		// delta=1 (as expected), but
		// offset is not 0, usually 0xFFFF; this leads
		// to a bounds fault in the offset code.
		if endCode[i] == 0xffff {
			offset[i] = 0
		}
		// Table case
		if offset[i] != 0 {
			computed_offset := int(rangeOffsetBase) + 2*i + int(offset[i])
			segmentLength := endCode[i] - startCode[i] + 1
			glyphIndex = make([]uint16, segmentLength)
			binary.Read(bytes.NewReader(b[computed_offset:]),
				binary.BigEndian, glyphIndex)
		}
		segments = append(segments, Segment(
			&Cmap4Segment{startCode[i],
				endCode[i], delta[i], offset[i],
				glyphIndex,
			}))
	}

	return segments
}

type Cmap12HeaderRecord struct {
	Format, Reserved uint16 // always 12
	Length           uint32
	Language         uint32
	NumGroups        uint32
}

// For Cmap Format 12
type Cmap12GroupRecord struct {
	StartCharCode, EndCharCode, StartGlyphID uint32
}

func (g *Cmap12GroupRecord) String() string {
	return fmt.Sprintf("char-start,%U,char-end,%U,glyph,%d", g.StartCharCode,
		g.EndCharCode, g.StartGlyphID)
}

func (g *Cmap12GroupRecord) Items() []CmapItem {
	if g.EndCharCode > 0x10ffff {
		log.Fatalf("Encountered invalid EndCharCode %x while processing cmap format 12 subtable", g.EndCharCode)
	}
	items := []CmapItem{}
	glyph := g.StartGlyphID
	for c := g.StartCharCode; c <= g.EndCharCode; c++ {
		items = append(items, CmapItem{rune(c), int(glyph)})
		glyph += 1
	}
	return items
}

func (g *Cmap12GroupRecord) Codes() []rune {
	// :todo: implement me
	return nil
}

func Format12Segments(b []byte) []Segment {
	r := bytes.NewReader(b)

	header := Cmap12HeaderRecord{}
	binary.Read(r, binary.BigEndian, &header)
	groups := make([]Cmap12GroupRecord, header.NumGroups)
	binary.Read(r, binary.BigEndian, groups)

	segments := []Segment{}
	for i := range groups {
		segments = append(segments, Segment(&groups[i]))
	}
	return segments
}

// Write a complete binary cmap with all subtables to `w`;
// and write a text representation to `text`.
// Note the lack of flex: the caller has no choice over
// what subtable encodings are used.
// 3 subtables are written with Platform/Encoding:
// - 0/4 format 12
// - 3/1 format 4
// - 3/10 format 12
// the format 12 subtables are shared by using the same offset.
func WriteCmap(w io.Writer, text io.Writer, charmap map[rune]int) {
	// https://docs.microsoft.com/en-gb/typography/opentype/spec/cmap
	// Format 12 is the simplest most general format.

	format12 := Format12(charmap)
	format4 := Format4(charmap)

	TextCmap(text, charmap)

	// 3 encodings; 2 subtables; 2 formats
	encodings := []EncodingRecord{
		{0, 4, 999},  // format 12
		{3, 1, 999},  // format 4
		{3, 10, 999}, // format 12
	}

	header := CmapHeaderRecord{0, uint16(len(encodings))}

	binary.Write(w, binary.BigEndian, header)

	// Compute offset of subtable and fix encoding records.
	offset := binary.Size(header) + binary.Size(encodings)
	offset4 := offset
	offset12 := offset4 + len(format4)

	encodings[0].Offset = uint32(offset12)
	encodings[1].Offset = uint32(offset4)
	encodings[2].Offset = uint32(offset12)

	binary.Write(w, binary.BigEndian, encodings)

	binary.Write(w, binary.BigEndian, format4)
	binary.Write(w, binary.BigEndian, format12)
}

// Compile a format 4 subtable.
// Note that this will silently reject any non-BMP codes
// (not a bug, modern tools do this).
// f: find part in spec where sentinel is required
// Returns the bytes for a cmap format 4 subtable
func Format4(charmap map[rune]int) []byte {

	// Similar to opentype.Cmap4Segment, but
	// this one is simpler and only used for compiling.
	// Note that this is a model, not a disk record.
	// In the binary file they are stored as 3 parallel arrays.
	type Format4Segment struct {
		StartCode, EndCode, StartGlyph uint16
	}
	// Convert charmap to a BMP codepoint uint16
	// to glyph index uint16 map.
	rmap := map[uint16]uint16{}
	for k, i := range charmap {
		if 0 < k && k <= 0xFFFF {
			rmap[uint16(k)] = uint16(i)
		}
	}

	// Sort the keys (which are codes, character codes)
	codes := []uint16{}
	for r, _ := range rmap {
		codes = append(codes, r)
	}
	sort.Slice(codes, func(i, j int) bool {
		return codes[i] < codes[j]
	})

	segments := []Format4Segment{}
	if len(codes) > 0 {
		pr := codes[0]                     // Previous rune
		pgi := rmap[pr]                    // Previous glyph
		seg := Format4Segment{pr, pr, pgi} // segment
		for i := 1; i < len(codes); i++ {
			// Determine if rune,glyph entry can be
			// added to existing group.
			r := codes[i]
			gi := rmap[r]
			if r == seg.EndCode+1 &&
				gi == seg.StartGlyph+r-seg.StartCode {
				seg.EndCode = r
			} else {
				segments = append(segments, seg)
				seg = Format4Segment{r, r, gi}
			}
		}
		segments = append(segments, seg)
	}

	// Add required sentinel. Per [TTREF]:
	// "To ensure that the search will terminate,
	//  the final endCode value must be 0xFFFF"
	segments = append(segments,
		Format4Segment{0xFFFF, 0xFFFF, 0})

	// Convert to three slices
	ends := make([]uint16, len(segments))
	starts := make([]uint16, len(segments))
	deltas := make([]uint16, len(segments))
	// Unused but required for binary file.
	rangeOffsets := make([]uint16, len(segments))
	for i, seg := range segments {
		ends[i] = seg.EndCode
		starts[i] = seg.StartCode
		deltas[i] = seg.StartGlyph - seg.StartCode
	}

	header := Format4HeaderRecord{}
	var padding uint16
	header.Format = 4
	len_codes := binary.Size(ends)
	// Compute length. Observing that all code arrays
	// (ends, starts, deltas, rangeOffsets) are same size.
	header.Length = uint16(binary.Size(&header) + 4*len_codes +
		binary.Size(&padding))
	header.SegCountX2 = uint16(len(segments) * 2)
	lg := FloorLog2(uint16(len(segments)))
	header.SearchRange = 2 * (1 << lg)
	header.EntrySelector = lg
	header.RangeShift = header.SegCountX2 - header.SearchRange

	w := &bytes.Buffer{}
	binary.Write(w, binary.BigEndian, &header)
	binary.Write(w, binary.BigEndian, ends)
	binary.Write(w, binary.BigEndian, &padding)
	binary.Write(w, binary.BigEndian, starts)
	binary.Write(w, binary.BigEndian, deltas)
	binary.Write(w, binary.BigEndian, rangeOffsets)

	bs := w.Bytes()
	if len(bs) != int(header.Length) {
		log.Fatalf("cmap format 4 wrote %d, but .Length is %d", len(bs), header.Length)
	}
	return bs
}

func FloorLog2(x uint16) uint16 {
	i := uint16(0)
	for ; x > 1; x >>= 1 {
		i++
	}
	return i
}

// Compile a cmap format 12 subtable
func Format12(charmap map[rune]int) []byte {
	w := &bytes.Buffer{}

	// Coerce map.

	rmap := map[uint32]uint32{}

	for k, i := range charmap {
		if k > 0 {
			rmap[uint32(k)] = uint32(i)
		}
	}

	// We need all the code points (runes) in sorted order.
	runes := []uint32{}
	for r, _ := range rmap {
		runes = append(runes, r)
	}
	sort.Slice(runes, func(i, j int) bool { return runes[i] < runes[j] })

	groups := []Cmap12GroupRecord{}
	if len(runes) > 0 {
		pr := runes[0]                      // Previous rune
		pgi := rmap[pr]                     // Previous glyph index
		g := Cmap12GroupRecord{pr, pr, pgi} // group
		for i := 1; i < len(runes); i++ {
			// Determine if rune,glyph entry can be
			// added to existing group.
			r := runes[i]
			gi := rmap[r]
			if r == g.EndCharCode+1 &&
				gi == g.StartGlyphID+r-g.StartCharCode {
				g.EndCharCode = r
			} else {
				groups = append(groups, g)
				g = Cmap12GroupRecord{r, r, gi}
			}
		}
		groups = append(groups, g)
	}

	length := uint32(binary.Size(Cmap12HeaderRecord{}) + binary.Size(groups))
	h := Cmap12HeaderRecord{12, 0, length, 0, uint32(len(groups))}
	binary.Write(w, binary.BigEndian, h)
	binary.Write(w, binary.BigEndian, groups)
	return w.Bytes()
}

// Write out to the file w the cmap in text format
func TextCmap(w io.Writer, charmap map[rune]int) {
	rmap := map[uint32]uint32{}

	for k, i := range charmap {
		rmap[uint32(k)] = uint32(i)
	}

	// We need all the code points (runes) in sorted order.
	runes := []uint32{}
	for r, _ := range rmap {
		runes = append(runes, r)
	}
	sort.Slice(runes, func(i, j int) bool { return runes[i] < runes[j] })

	i := 0
	for i < len(runes) {
		r := runes[i]                    // Current rune
		gi := rmap[r]                    // Glyph index of rune
		g := Cmap12GroupRecord{r, r, gi} // group

		for {
			i++
			if i >= len(runes) {
				break
			}
			// Determine if rune,glyph entry can be
			// added to existing group.
			r = runes[i]
			gi = rmap[r]
			if r == g.EndCharCode+1 &&
				gi == g.StartGlyphID+r-g.StartCharCode {
				g.EndCharCode = r
			} else {
				break
			}
		}
		fmt.Fprintf(w, "cmap,char,%U,%U,glyph,%d\n",
			g.StartCharCode, g.EndCharCode,
			g.StartGlyphID)
	}
}
