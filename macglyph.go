package opentype

import (
_"embed"
)

//go:embed mac-standard-glyph.txt
var MacStandardGlyphStrings string

var MacStandardGlyph = CFFSliceStrings(MacStandardGlyphStrings)
