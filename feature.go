package opentype

// The types and constructors relevant to features,
// referring to OpenType features in GSUB and GPOS tables.

import (
	"bytes"
	"encoding/binary"
)

// https://docs.microsoft.com/en-gb/typography/opentype/spec/chapter2#slTbl_sRec
type ScriptRecord struct {
	ScriptTag    [4]byte
	ScriptOffset uint16
}

// https://docs.microsoft.com/en-gb/typography/opentype/spec/chapter2#script-table-and-language-system-record
// Parsed by ScripTable function.
type ScriptTableRecord struct {
	DefaultLangSysOffset uint16
	LangSysCount         uint16
	// LangSysRecord array follows
}

type LangSysRecord struct {
	LangSysTag    [4]byte
	LangSysOffset uint16
}

// https://docs.microsoft.com/en-gb/typography/opentype/spec/chapter2#language-system-table
type LangSysTableRecord struct {
	LookupOrder          uint16
	RequiredFeatureIndex uint16
	FeatureIndexCount    uint16
}

// Model for a Script.
// Each script has a name which is a 4 letter tag, see https://docs.microsoft.com/en-gb/typography/opentype/spec/scripttags.
// A Script consists of a list of languages (Language System, really),
// each language identified by a four-character code,
// and an additional and optional default language system
// (which need not correspond to any other actual language, but
// is commonly referred to using the reserved code `dflt`).
type Script struct {
	Tag         string
	DefaultLang *Lang
	Lang        []Lang
}

// Model for a Language System. The OpenType spec mostly uses
// the term Language System instead of language because there
// may not be a one-to-one mapping (for example Japanese has
// several typographic conventions for certain glyphs according
// to historical periods of standardisation).
// Here though, i use the type Lang, on the understanding that
// this actually corresponds to an OpenType LangSys.
// Typographically there is no need to care about the
// distinction, there is simply a four-character code that is
// either in use or not.
// The required feature is optional (nil when not present).
// The Feature slice is a list of the features, each one being
// represented by its FeatureIndex.
type Lang struct {
	Tag             string
	RequiredFeature *int
	Feature         []int
}

// Convert the binary ScriptList table to a modelled Script slice.
func Scripts(bs []byte) []Script {
	var scriptCount uint16
	r := bytes.NewReader(bs)

	// Read ScriptList table, which is a count,
	// followed by a ScriptRecord array.
	binary.Read(r, binary.BigEndian, &scriptCount)
	scriptRecords := make([]ScriptRecord, int(scriptCount))
	binary.Read(r, binary.BigEndian, scriptRecords)

	// Convert each ScriptRecord to Script.
	scripts := make([]Script, len(scriptRecords))
	for i := range scriptRecords {
		scripts[i].Tag = string(scriptRecords[i].ScriptTag[:])
		scripts[i].DefaultLang, scripts[i].Lang = ScriptTable(bs[int(scriptRecords[i].ScriptOffset):])
	}
	return scripts
}

// Convert binary representation of a Script table to
// modelled Lang structs.
// The return value is the optional default language and a
// sequence of languages.
func ScriptTable(bs []byte) (*Lang, []Lang) {
	// The OpenType spec introduces some potential for
	// confusion here. The Script table is actually a table
	// of LangSys entries.
	header := ScriptTableRecord{}
	r := bytes.NewReader(bs)
	binary.Read(r, binary.BigEndian, &header)

	langSysRecords := make([]LangSysRecord, int(header.LangSysCount))
	binary.Read(r, binary.BigEndian, langSysRecords)

	var dflt *Lang
	if header.DefaultLangSysOffset != 0 {
		dflt = &Lang{}
		dflt.Tag = "dflt"
		dflt.RequiredFeature, dflt.Feature =
			LangSysFeatures(bs[int(header.DefaultLangSysOffset):])
	}

	langs := make([]Lang, len(langSysRecords))
	for i, record := range langSysRecords {
		langs[i].Tag = string(record.LangSysTag[:])
		langs[i].RequiredFeature, langs[i].Feature =
			LangSysFeatures(bs[int(record.LangSysOffset):])
	}
	return dflt, langs
}

// Convert binary representation of a LangSys table (a slice of bytes) to
// the optional required feature and the feature index list.
// Required feature is nil if no required feature (represented
// as 65535 in the binary).
func LangSysFeatures(bs []byte) (*int, []int) {
	r := bytes.NewReader(bs)
	header := LangSysTableRecord{}
	binary.Read(r, binary.BigEndian, &header)

	indices := make([]uint16, header.FeatureIndexCount)
	binary.Read(r, binary.BigEndian, &indices)

	required := (*int)(nil)
	rfi := int(header.RequiredFeatureIndex)
	if rfi != 65535 {
		required = &rfi
	}

	result := []int{}
	for i := range indices {
		result = append(result, int(indices[i]))
	}

	return required, result
}

// A feature is a tag (generally 4 ASCII bytes, such as "kern" or "liga"),
// and its lookups (each lookup identified by a small integer,
// the lookups themselves are stored elsewhere).
type Feature struct {
	Tag    string
	Lookup []int
}

type FeatureRecord struct {
	Tag           [4]byte
	FeatureOffset uint16
}

type FeatureTableRecord struct {
	FeatureParams, LookupIndexCount uint16
}

// https://docs.microsoft.com/en-gb/typography/opentype/spec/chapter2#feature-list-table
// Convert binary representation of Feature List Table to slice of Feature.
func Features(bs []byte) []Feature {
	r := bytes.NewReader(bs)
	var featureCount uint16
	binary.Read(r, binary.BigEndian, &featureCount)

	featureRecords := make([]FeatureRecord, featureCount)
	binary.Read(r, binary.BigEndian, featureRecords)

	features := make([]Feature, len(featureRecords))
	for i, record := range featureRecords {
		features[i].Tag = string(record.Tag[:])
		features[i].Lookup = FeatureLookups(bs[record.FeatureOffset:])
	}
	return features
}

// Convert binary representation of Feature Table into a list of lookups
// (Each lookup being an integer).
// Ignores the feature params (which are only used for certain features, see
// https://docs.microsoft.com/en-gb/typography/opentype/spec/chapter2#feature-table
func FeatureLookups(bs []byte) []int {
	r := bytes.NewReader(bs)
	header := FeatureTableRecord{}
	binary.Read(r, binary.BigEndian, &header)

	uints := make([]uint16, header.LookupIndexCount)
	binary.Read(r, binary.BigEndian, uints)

	ints := make([]int, len(uints))
	for i := range uints {
		ints[i] = int(uints[i])
	}
	return ints
}
