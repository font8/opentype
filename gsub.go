package opentype

// GSUB Lookups (script / lang / feature types are in feature.go)
// Some of the code (ChainedSequenceContext and so on) is
// common to GPOS and GSUB lookups, so should be moved.

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"log"
)

// Model a Lookup table.
// https://docs.microsoft.com/en-gb/typography/opentype/spec/chapter2#lookup-table
type Lookup struct {
	LookupRecord
	SubTable []interface{}
}

type LookupRecord struct {
	LookupType, LookupFlag, SubTableCount uint16
}

func LookupGlyph(lookup Lookup, glyph int) (int, error) {
	for i := range lookup.SubTable {
		switch sub := lookup.SubTable[i].(type) {
		case SingleSubst2:
			t, ok := sub.Substitute[glyph]
			if ok {
				return t, nil
			}
		case SingleSubst1:
			return sub.Apply(glyph), nil
		default:
			return 0, fmt.Errorf("Unimplemented Lookup %T %v",
				lookup.SubTable[i],
				lookup)

		}
	}
	return 0, fmt.Errorf("Glyph %d not covered; Lookup %v", glyph, lookup)
}

// lookup should be a Lookup with LigatureSubst sub-table entries.
// input should be a proposed sequence of input glyphs.
// Result is a ligature if the lookup applied (0 if not);
// the acted bool is true if and only if the lokup applied;
// error is non-nil when there are errors.
func LookupLigatureApply(lookup Lookup, input []int) (
	liga int, acted bool, err error,
) {
	for i := range lookup.SubTable {
		switch sub := lookup.SubTable[i].(type) {
		case LigatureSubst:
			liga, ok := LigatureSubstApply(sub, input)
			if ok {
				return liga, true, nil
			}
		default:
			return 0, false, fmt.Errorf("Do not call LookupLigatureSubst with Lookup %T %v",
				lookup.SubTable[i], lookup)
		}
	}
	return 0, false, nil
}

func LigatureSubstApply(sub LigatureSubst, input []int) (int, bool) {
	// Try all the sets, even though we could use the coverage
	// table to narrow down the selection.
	for covi := range sub.Coverage {
		set := sub.LigatureSets[covi]
		for _, ligature := range set {
			// Try and match all the components of the ligature to
			// the input.
			if len(ligature.Component) > len(input) {
				continue
			}
			for ci, c := range ligature.Component {
				if c != input[ci] {
					continue
				}
			}
			// All components matched
			return ligature.Ligature, true
		}
	}
	return 0, false
}

type GsubUnknown struct {
	LookupType uint16
	bs         []byte
}

// Return a slice of Lookup, given the bytes for the
// Lookup List Table (and all bytes following).
// https://docs.microsoft.com/en-gb/typography/opentype/spec/chapter2#lookup-list-table
func GsubLookups(bs []byte) []Lookup {
	r := bytes.NewReader(bs)
	var lookupCount uint16
	binary.Read(r, binary.BigEndian, &lookupCount)

	offsets := make([]uint16, lookupCount)
	binary.Read(r, binary.BigEndian, offsets)

	lookups := make([]Lookup, len(offsets))

	for i, offset := range offsets {
		lookups[i] = GsubLookupSub(bs[offset:])
	}
	return lookups
}

// Returns a Lookup.
// A word of warning: 1 Lookup Table has 1 Lookup Type;
// all the subtables are of the same Lookup Type;
// this Lookup Type is store with the Lookup Table,
// not the subtables.
// The Lookup Types are a small integer that varies
// between GPOS and GSUB TrueType tables.
func GsubLookupSub(bs []byte) Lookup {
	r := bytes.NewReader(bs)
	header := &LookupRecord{}
	binary.Read(r, binary.BigEndian, header)
	subtableOffsets := make([]uint16, header.SubTableCount)
	binary.Read(r, binary.BigEndian, subtableOffsets)

	subTables := make([]interface{}, len(subtableOffsets))

	for i, offset := range subtableOffsets {
		subTables[i] = GsubSubTableType(header.LookupType, bs[offset:])
	}

	return Lookup{*header, subTables}
}

// Each GSUB Lookup sub table must be of the same type,
// described by a small enumerated int.
// https://docs.microsoft.com/en-us/typography/opentype/spec/gsub#table-organization
func GsubSubTableType(lookupType uint16, bs []byte) interface{} {
	switch lookupType {
	case 1:
		return SingleSubstitution(bs)
	case 2:
		return MultipleSubstitution(bs)
	case 3:
		return AlternateSubstitution(bs)
	case 4:
		return LigatureSubstitution(bs)
	case 6:
		return ChainedSequenceContext(bs)
	}
	return GsubUnknown{lookupType, bs}
}

// Coverage

// https://docs.microsoft.com/en-us/typography/opentype/spec/chapter2#coverage-table

type CoverageTableHeader struct {
	CoverageFormat uint16 // 1 or 2
	Count          uint16 // Number of items following
	// each item following is either:
	// 1) uint16 glyph index, or;
	// 2) CoverageRangeRecord
}

// https://docs.microsoft.com/en-gb/typography/opentype/spec/chapter2#coverage-format-2
type CoverageRangeRecord struct {
	StartGlyphID, EndGlyphID, StartCoverageIndex uint16
}

type CoverageTable []int

// Convert binary repreresentation of Coverage table to
// a simple slice of glyph indexes.
func Coverage(bs []byte) CoverageTable {
	gs := []int{}
	r := bytes.NewReader(bs)

	header := CoverageTableHeader{}
	binary.Read(r, binary.BigEndian, &header)
	switch header.CoverageFormat {
	case 1:
		glyphs := make([]uint16, header.Count)
		binary.Read(r, binary.BigEndian, glyphs)
		for _, gi := range glyphs {
			gs = append(gs, int(gi))
		}
	case 2:
		ranges := make([]CoverageRangeRecord, header.Count)
		binary.Read(r, binary.BigEndian, ranges)
		for _, r := range ranges {
			for gi := r.StartGlyphID; gi <= r.EndGlyphID; gi++ {
				gs = append(gs, int(gi))
			}
		}
	default:
		log.Print("Unknown coverage table format",
			header.CoverageFormat, bs[:999])
		return nil
	}
	return gs
}

// Single subst
// https://docs.microsoft.com/en-us/typography/opentype/spec/gsub#lookuptype-1-single-substitution-subtable

type SingleSubst struct {
	Format uint16
	U      [2]uint16
}

type SingleSubst1Record struct {
	Format         uint16
	CoverageOffset uint16
	Delta          int16
}

type SingleSubst1 struct {
	Format   int
	Coverage CoverageTable
	Delta    int
}

type SingleSubst2Record struct {
	Format         uint16
	CoverageOffset uint16
	GlyphCount     uint16
}

type SingleSubst2 struct {
	Format int
	// Combines the CoverageTable and the substitute glyph array
	// into a single map.
	Substitute      map[int]int
	Coverage        CoverageTable
	SubstituteGlyph []int
}

func SingleSubstitution(bs []byte) interface{} {
	r := bytes.NewReader(bs)
	subst := SingleSubst{}
	binary.Read(r, binary.BigEndian, &subst)
	switch subst.Format {
	case 1:
		return SingleSubstitutionFormat1(bs)
	// https://docs.microsoft.com/en-us/typography/opentype/spec/gsub#12-single-substitution-format-2
	case 2:
		return SingleSubstitutionFormat2(bs)
	default:
		return subst
	}
}

// Apply the SingleSubst1 to a glyph, transforming it.
func (sub *SingleSubst1) Apply(glyph int) int {
	for _, g := range sub.Coverage {
		if g == glyph {
			return int(uint16(glyph) + uint16(sub.Delta))
		}
	}
	return glyph
}

func SingleSubstitutionFormat1(bs []byte) SingleSubst1 {
	header := SingleSubst1Record{}
	r := bytes.NewReader(bs)
	binary.Read(r, binary.BigEndian, &header)

	if header.Format != 1 {
		log.Fatal("SingleSubstitution format wrong:", header)
	}

	subst := SingleSubst1{}
	subst.Format = 1
	subst.Coverage = Coverage(bs[header.CoverageOffset:])
	subst.Delta = int(header.Delta)
	return subst
}

func SingleSubstitutionFormat2(bs []byte) SingleSubst2 {
	header := SingleSubst2Record{}
	r := bytes.NewReader(bs)
	binary.Read(r, binary.BigEndian, &header)

	if header.Format != 2 {
		log.Fatal("SingleSubstitution format wrong:", header)
	}

	substitutions := make([]uint16, header.GlyphCount)
	binary.Read(r, binary.BigEndian, substitutions)

	res := SingleSubst2{}
	res.Format = int(header.Format)
	res.Substitute = map[int]int{}
	res.Coverage = Coverage(bs[header.CoverageOffset:])
	res.SubstituteGlyph = make([]int, header.GlyphCount)
	for i := range substitutions {
		res.SubstituteGlyph[i] = int(substitutions[i])
	}
	for i, glyph := range res.Coverage {
		res.Substitute[int(glyph)] = res.SubstituteGlyph[i]
	}
	return res
}

// Multiple Substitution

// https://docs.microsoft.com/en-us/typography/opentype/spec/gsub#21-multiple-substitution-format-1

type MultipleSubst struct {
	Coverage  CoverageTable
	Sequences []Sequence
}

type MultipleSubstRecord struct {
	Format         uint16
	CoverageOffset uint16
	SequenceCount  uint16
}

// Sequence of glyphs
type Sequence []int

func MultipleSubstitution(bs []byte) MultipleSubst {
	header := MultipleSubstRecord{}
	r := bytes.NewReader(bs)
	binary.Read(r, binary.BigEndian, &header)

	if header.Format != 1 {
		log.Fatal("MultipleSubstitution format wrong:", header)
	}

	offsets := make([]uint16, header.SequenceCount)
	binary.Read(r, binary.BigEndian, offsets)

	res := MultipleSubst{}
	res.Coverage = Coverage(bs[header.CoverageOffset:])

	for _, offset := range offsets {
		res.Sequences = append(res.Sequences, SequenceMake(bs[offset:]))
	}

	return res
}

// Multiple Substitution Format 1, Sequence table, binary representation to model.
// :todo: same as AlternateSetMake?
func SequenceMake(bs []byte) Sequence {
	count := binary.BigEndian.Uint16(bs[:2])
	ids := make([]uint16, count)
	binary.Read(bytes.NewReader(bs[2:]), binary.BigEndian, ids)

	glyphs := make([]int, count)
	for i := range ids {
		glyphs[i] = int(ids[i])
	}
	return glyphs
}

// Alternate Substitution

// https://docs.microsoft.com/en-us/typography/opentype/spec/gsub#lookuptype-3-alternate-substitution-subtable

type AlternateSubst struct {
	Coverage      CoverageTable
	AlternateSets []AlternateSet
}

type AlternateSubstRecord struct {
	Format            uint16
	CoverageOffset    uint16
	AlternateSetCount uint16
}

type AlternateSet []int

func AlternateSubstitution(bs []byte) interface{} {
	r := bytes.NewReader(bs)
	header := AlternateSubstRecord{}
	binary.Read(r, binary.BigEndian, &header)

	switch header.Format {
	case 1:
		return AlternateSubstitutionFormat1(&header, r, bs)
	default:
		// Would be unexpected. Only Format 1 is defined.
		log.Printf("Unknown Alternate Substitution Format: %v\n", header)
		return header
	}
}

func AlternateSubstitutionFormat1(header *AlternateSubstRecord,
	r io.Reader,
	bs []byte,
) interface{} {
	alt := AlternateSubst{}
	alt.Coverage = Coverage(bs[header.CoverageOffset:])

	offsets := make([]uint16, header.AlternateSetCount)
	binary.Read(r, binary.BigEndian, offsets)

	for _, offset := range offsets {
		alt.AlternateSets = append(alt.AlternateSets, AlternateSetMake(bs[offset:]))
	}
	return alt
}

func AlternateSetMake(bs []byte) AlternateSet {
	count := binary.BigEndian.Uint16(bs[:2])
	ids := make([]uint16, count)
	binary.Read(bytes.NewReader(bs[2:]), binary.BigEndian, ids)

	alts := make([]int, count)
	for i := range ids {
		alts[i] = int(ids[i])
	}
	return alts
}

// Ligature Substitution

// https://docs.microsoft.com/en-us/typography/opentype/spec/gsub#lookuptype-4-ligature-substitution-subtable

// https://docs.microsoft.com/en-us/typography/opentype/spec/gsub#41-ligature-substitution-format-1
// As of 2021-11-26, only one format
type LigatureSubstRecord struct {
	Format           uint16
	CoverageOffset   uint16
	LigatureSetCount uint16
}

type LigatureSubst struct {
	Coverage     CoverageTable
	LigatureSets []LigatureSet
}

type LigatureSet []Ligature

type Ligature struct {
	Ligature int // a GlyphID
	// GlyphIDs of the components _including the initial glyph_
	Component []int
}

func LigatureSubstitution(bs []byte) interface{} {
	r := bytes.NewReader(bs)
	header := LigatureSubstRecord{}
	binary.Read(r, binary.BigEndian, &header)
	switch header.Format {
	// https://docs.microsoft.com/en-us/typography/opentype/spec/gsub#41-ligature-substitution-format-1
	case 1:
		return LigatureSubstitutionFormat1(&header, r, bs)
	default:
		// Would be unexpected. Only Format 1 is defined.
		log.Printf("Unknown Ligature Substitution Format: %v\n", header)
		return header
	}
}

func LigatureSubstitutionFormat1(header *LigatureSubstRecord, r io.Reader, bs []byte) LigatureSubst {
	subst := LigatureSubst{}
	subst.Coverage = Coverage(bs[header.CoverageOffset:])
	offsets := make([]uint16, header.LigatureSetCount)
	binary.Read(r, binary.BigEndian, offsets)

	for _, offset := range offsets {
		subst.LigatureSets = append(subst.LigatureSets, LigatureSetMake(bs[offset:]))
	}

	// For convenience (of the code using this structure),
	// The initial component glyph,
	// which is the same for each component sequence in a LigatureSet,
	// is prepended to each component sequence in the set.
	for i := range subst.Coverage {
		initial := subst.Coverage[i]
		set := subst.LigatureSets[i]
		for l := range set {
			set[l].Component = append([]int{initial}, set[l].Component...)
		}
	}

	return subst
}

func LigatureSetMake(bs []byte) LigatureSet {
	set := LigatureSet{}
	// There no Record struct for this, it's a simple uint16 count, followed by an
	// array of uint16 offsets.
	count := binary.BigEndian.Uint16(bs[:2])

	// Offsets to ligatures.
	offsets := make([]uint16, count)
	binary.Read(bytes.NewReader(bs[2:]), binary.BigEndian, offsets)

	for _, offset := range offsets {
		set = append(set, LigatureMake(bs[offset:]))
	}
	return set
}

func LigatureMake(bs []byte) Ligature {
	liga := Ligature{}
	header := struct{ Ligature, Count uint16 }{}

	r := bytes.NewReader(bs)
	binary.Read(r, binary.BigEndian, &header)
	liga.Ligature = int(header.Ligature)

	glyphs := make([]uint16, header.Count-1)
	binary.Read(r, binary.BigEndian, glyphs)
	for _, g := range glyphs {
		liga.Component = append(liga.Component, int(g))
	}
	return liga
}

// Contextual Lookup Subtables

// ChainedSequenceContext shared by GPOS type 8 and GSUB type 6
type ChainedSequence struct {
	Format uint16
	U      [3]uint16
}

func ChainedSequenceContext(bs []byte) interface{} {
	r := bytes.NewReader(bs)
	ch := &ChainedSequence{}
	binary.Read(r, binary.BigEndian, ch)
	switch ch.Format {
	case 1:
		return ChainedSequenceContextFormat1(bs)
	case 3:
		return ChainedSequenceContextFormat3(bs)
	}
	return *ch
}

// A pair that models the Sequence Lookup Record
type SequenceLookup struct {
	// Index into input
	SequenceIndex int
	// Lookup to be applied at that point in the input
	LookupIndex int
}

// https://docs.microsoft.com/en-us/typography/opentype/spec/chapter2#sequence-lookup-record
// Specifies an action to be applied to a glyph in the input
// sequence (for a variety of contextual lookups).
// The SequenceIndex is the target of the action;
// the LookupListIndex specifies (nested/recursively) a lookup
// to apply.
type SequenceLookupRecord struct {
	SequenceIndex, LookupListIndex uint16
}

// https://learn.microsoft.com/en-us/typography/opentype/spec/chapter2#chained-sequence-context-format-1-simple-glyph-contexts
type ChainedSequenceContext1Record struct {
	Format                 uint16 // 1 in this case
	CoverageOffset         uint16 // from beginning of this record
	ChainedSeqRuleSetCount uint16
	// ChainedSeqRulesetOffsets[...]
}

type ChainedSequence1 struct {
	Format   int
	Coverage CoverageTable
	// 1 ruleset per covered glyph (notionally, according to
	// spec tho, the number of covered glyphs and the number of
	// rulesets should agree, but is not required).
	ChainedSeqRuleSet []ChainedSeqRuleSet
}

type ChainedSeqRuleSet []ChainedSeqRule

type ChainedSeqRule struct {
	// The slice elements are Glyph IDs
	// :todo: check correct order on backtrack (it's reversed)
	Backtrack       []int
	Input           []int
	Lookahead       []int
	SequenceLookups []SequenceLookup
}

// https://learn.microsoft.com/en-us/typography/opentype/spec/chapter2#chained-sequence-context-format-1-simple-glyph-contexts
func ChainedSequenceContextFormat1(bs []byte) ChainedSequence1 {
	r := bytes.NewReader(bs)
	ch1 := ChainedSequence1{}

	conrec := ChainedSequenceContext1Record{}
	binary.Read(r, binary.BigEndian, &conrec)

	if conrec.Format != 1 {
		log.Print("GSUB Format Problem, ChainedSequenceContextFormat1 called, but format is %v", conrec.Format)
		// :todo: consider using an error type here?
		return ChainedSequence1{}
	}

	ch1.Coverage = Coverage(bs[conrec.CoverageOffset:])

	rulesetOffsets := make([]uint16,
		conrec.ChainedSeqRuleSetCount)
	binary.Read(r, binary.BigEndian, rulesetOffsets)

	for _, u := range rulesetOffsets {
		// deliberately shadow bs
		bs := bs[u:]
		rulecount := binary.BigEndian.Uint16(bs[:2])
		ruleoffsets := make([]uint16, rulecount)
		binary.Read(bytes.NewReader(bs[2:]), binary.BigEndian, ruleoffsets)

		ruleset := ChainedSeqRuleSet{}
		for _, u := range ruleoffsets {
			rule := ChainedSequenceRule(bs[u:])
			ruleset = append(ruleset, rule)
		}
		ch1.ChainedSeqRuleSet = append(ch1.ChainedSeqRuleSet,
			ruleset)
	}

	return ch1
}

// Return a single modelled ChainedSeqRule from the binary
// representation
func ChainedSequenceRule(bs []byte) ChainedSeqRule {
	r := bytes.NewReader(bs)

	var backtrackGlyphCount uint16
	binary.Read(r, binary.BigEndian, &backtrackGlyphCount)
	backtrack := make([]uint16, backtrackGlyphCount)
	binary.Read(r, binary.BigEndian, backtrack)

	var inputGlyphCount uint16
	binary.Read(r, binary.BigEndian, &inputGlyphCount)
	// The -1 is correct.
	// The corresponding CoverageTable gives the first glyph implicity.
	input := make([]uint16, inputGlyphCount-1)
	binary.Read(r, binary.BigEndian, input)

	var lookaheadGlyphCount uint16
	binary.Read(r, binary.BigEndian, &lookaheadGlyphCount)
	lookahead := make([]uint16, lookaheadGlyphCount)
	binary.Read(r, binary.BigEndian, lookahead)

	var seqLookupCount uint16
	binary.Read(r, binary.BigEndian, &seqLookupCount)
	seqLookupRecords := make([]SequenceLookupRecord, seqLookupCount)
	binary.Read(r, binary.BigEndian, seqLookupRecords)

	rule := ChainedSeqRule{}

	rule.Backtrack = make([]int, len(backtrack))
	for i := range backtrack {
		rule.Backtrack[i] = int(backtrack[i])
	}

	rule.Input = make([]int, len(input))
	for i := range input {
		rule.Input[i] = int(input[i])
	}

	rule.Lookahead = make([]int, len(lookahead))
	for i := range lookahead {
		rule.Lookahead[i] = int(lookahead[i])
	}

	SequenceLookups := make([]SequenceLookup, seqLookupCount)
	for i := range seqLookupRecords {
		SequenceLookups[i].SequenceIndex =
			int(seqLookupRecords[i].SequenceIndex)
		SequenceLookups[i].LookupIndex =
			int(seqLookupRecords[i].LookupListIndex)
	}
	rule.SequenceLookups = SequenceLookups

	return rule
}

// https://docs.microsoft.com/en-us/typography/opentype/spec/chapter2#chained-sequence-context-format-3-coverage-based-glyph-contexts
type ChainedSequence3 struct {
	Format int
	// :todo: check correct order on backtrack (it's reversed)
	BacktrackCoverage []CoverageTable
	InputCoverage     []CoverageTable
	LookaheadCoverage []CoverageTable
	SequenceLookups   []SequenceLookup
}

// https://docs.microsoft.com/en-us/typography/opentype/spec/chapter2#chained-sequence-context-format-3-coverage-based-glyph-contexts
// Parse a Chained Sequence Context in Format 3 and
// return its model
func ChainedSequenceContextFormat3(bs []byte) ChainedSequence3 {
	r := bytes.NewReader(bs)
	ch3 := ChainedSequence3{}

	var format uint16
	binary.Read(r, binary.BigEndian, &format)

	var backtrackGlyphCount uint16
	binary.Read(r, binary.BigEndian, &backtrackGlyphCount)
	backtrackCoverageOffsets := make([]uint16, backtrackGlyphCount)
	binary.Read(r, binary.BigEndian, backtrackCoverageOffsets)

	var inputGlyphCount uint16
	binary.Read(r, binary.BigEndian, &inputGlyphCount)
	inputCoverageOffsets := make([]uint16, inputGlyphCount)
	binary.Read(r, binary.BigEndian, inputCoverageOffsets)

	var lookaheadGlyphCount uint16
	binary.Read(r, binary.BigEndian, &lookaheadGlyphCount)
	lookaheadCoverageOffsets := make([]uint16, lookaheadGlyphCount)
	binary.Read(r, binary.BigEndian, lookaheadCoverageOffsets)

	ch3.Format = int(format)
	ch3.BacktrackCoverage = make([]CoverageTable, len(backtrackCoverageOffsets))
	for i, u := range backtrackCoverageOffsets {
		ch3.BacktrackCoverage[i] = Coverage(bs[u:])
	}
	ch3.InputCoverage = make([]CoverageTable, len(inputCoverageOffsets))
	for i, u := range inputCoverageOffsets {
		ch3.InputCoverage[i] = Coverage(bs[u:])
	}
	ch3.LookaheadCoverage = make([]CoverageTable, len(lookaheadCoverageOffsets))
	for i, u := range lookaheadCoverageOffsets {
		ch3.LookaheadCoverage[i] = Coverage(bs[u:])
	}

	var seqLookupCount uint16
	binary.Read(r, binary.BigEndian, &seqLookupCount)
	seqLookupRecords := make([]SequenceLookupRecord, seqLookupCount)
	binary.Read(r, binary.BigEndian, seqLookupRecords)

	SequenceLookups := make([]SequenceLookup, seqLookupCount)
	for i := range seqLookupRecords {
		SequenceLookups[i].SequenceIndex =
			int(seqLookupRecords[i].SequenceIndex)
		SequenceLookups[i].LookupIndex =
			int(seqLookupRecords[i].LookupListIndex)
	}

	ch3.SequenceLookups = SequenceLookups

	return ch3
}
