package opentype

import (
	_ "embed"
	"strings"
)

//go:embed cff-standard-strings.txt
var CFFStrings string

var CFFStandardString = CFFSliceStrings(CFFStrings)

// The format of the above file is a series of lines:
/*
0 .notdef
1 space
2 exclam
3 quotedbl
*/

func CFFSliceStrings(s string) []string {
	ss := []string{}
	for _, row := range strings.Split(s, "\n") {
		if len(row) == 0 {
			break
		}
		ss = append(ss, strings.Fields(row)[1])
	}
	return ss
}
