package opentype

// Glyph Metrics (as opposed to font-wide Font Metrics)

import (
	"bytes"
	"encoding/binary"
)

// https://docs.microsoft.com/en-gb/typography/opentype/spec/hhea
type HorizontalHeaderRecord struct {
	MajorVersion, MinorVersion                          uint16
	Ascender, Descender, LineGap                        int16
	AdvanceWidthMax                                     uint16
	MinLeftSideBearing, MinRightSideBearing, XMaxExtent int16
	CaretSlopeRise, CaretSlopeRun, CaretOffset          int16
	Reserved                                            [4]int16
	MetricDataFormat                                    int16
	NumberOfHMetrics                                    uint16
}

// https://docs.microsoft.com/en-gb/typography/opentype/spec/hmtx
type LongMetricRecord struct {
	Advance uint16
	Bearing int16
}

// Metric to be used for either Horizontal or Vertical metric.
// For Horizontal:
// - Advance is Advance Width
// - Bearing is Left Side Bearing
// For Vertical:
// - Advance is Advance Height
// - Bearing is Top Bearing
// These follow hmtx and vmtx in OpenType.
// Occasionally the OpenType spec calls the Advance "Escapement".
type Metric struct {
	Advance, Bearing int
}

func (font *T) HMetrics() ([]Metric, error) {
	hmtx, ok := font.Table["hmtx"]
	if !ok {
		return nil, nil
	}
	hhea, ok := font.Table["hhea"]
	if !ok {
		return nil, nil
	}
	header := HorizontalHeaderRecord{}
	err := binary.Read(bytes.NewReader(hhea.B), binary.BigEndian, &header)
	if err != nil {
		return nil, err
	}

	m := int(header.NumberOfHMetrics)
	N, _ := font.GlyphCount()

	// m LongMetricRecord
	ms := make([]LongMetricRecord, m)
	// and the remainder are just bearings
	bearings := make([]int16, N-m)
	buf := bytes.NewReader(hmtx.B)
	err = binary.Read(buf, binary.BigEndian, ms)
	if err != nil {
		return nil, err
	}
	err = binary.Read(buf, binary.BigEndian, bearings)
	if err != nil {
		return nil, err
	}

	metrics := make([]Metric, N)
	for i := 0; i < N; i++ {
		lastAdvance := -1
		if i < len(ms) {
			metrics[i].Advance = int(ms[i].Advance)
			metrics[i].Bearing = int(ms[i].Bearing)
			lastAdvance = metrics[i].Advance
		} else {
			j := i - m
			metrics[i].Advance = lastAdvance
			metrics[i].Bearing = int(bearings[j])
		}
	}
	return metrics, nil
}

func (font *T) VMetrics() ([]Metric, error) {
	return nil, nil
}
