package opentype

// Code that supports Adobe Type 2 Charstring Format

// REFERENCES

// [Type2Spec] https://adobe-type-tools.github.io/font-tech-notes/pdfs/5177.Type2.pdf
//   The Type 2 Specification, essential reading and reference.

// [j00ru2015] https://j00ru.vexillium.org/slides/2015/44con.pdf
//   "Reverse engineering and exploiting font rasterizers"
//   This report provides an entertaining read and
//   some hints that some of the currently reserved Charstring operators
//   are in fact deprecated operators from earlier versions, and that
//   of course they are implemented (in Adobe rendering systems).

// Conversion to Outline

// You have to interpret the (PostScript-like) Charstring byte codes.
// And it has to be a real interpreter that handles callsubr and
// dup and ifelse.
// Even parsing the stream into literals and operators is
// tricky because the `hintmask` operator consumes a number
// of following bytes;
// the number of bytes consumed depends on the number of hints.
// If hint operators were allowed anywhere this would be a problem,
// because we could not easily (or possibly statically)
// determine the number of hints.
// But, the restrictions on the Charstring organisation in
// Section 3.1 of [Type2Spec] mean that all of the hints must
// precede any path or computation operators.
// That means that we can determine the number of hints,
// but since they might appear inside subrs we still have
// to effective run subrs.

// Useful test materials

// font/Ouroboros
//   - `multiply` is as plain as possible: no subr, no mask
//   - `two` has mask, but no subr
// Worth looking at for longer vhcurveto (and other operators)
//   - `eight` has a longer (12) vhcurveto (no subr, has mask)
//   - `heart` has a longer vhcurveto vvcurveto (no subr) hvcurveto vlineto
//   - `infinity` vhcurveto rcurveline hhcurveto hvcurveto (long)
//   - `u1F71A` rlinecurve
//   - `bullet` is a nice one, a circle drawn with a single vhcurveto
//   - `uni261B` (manicule)
//   - `u1F74D` (Alchemical tutty) has hmoveto
// font/ChunkFive
//   - `Aring` requires vmoveto

import (
	"encoding/binary"
	"fmt"
	"gitlab.com/font8/coff"
	"log"
	"math"
	"strings"
)

type CFF2Glyph struct {
	font       *T
	fontIndex  int // Index within FontSet; 0 for now
	glyphIndex int
}

func (g *CFF2Glyph) ToOutline() *coff.Outline {
	bs := g.font.GlyphData[g.glyphIndex]
	log.Print(bs)
	gsubr := g.font.cff.Gsubr
	// :todo: Implement fonts within Font Sets.
	if g.fontIndex != 0 {
		return nil
	}
	subr := g.font.cff.Subr[g.fontIndex]

	InterpretCharstring(bs, subr, gsubr, ActionPrint)
	action, outline := MakeActionOutline()
	InterpretCharstring(bs, subr, gsubr, action)
	outline.Close()
	return outline
}

// A minimal interpreter that can't draw.
type Terp struct {
	Subr      [][]byte // Set once, never changes
	Gsubr     [][]byte // Set once, never changes
	BiasSubr  int
	BiasGsubr int
	PC
	// Total number of callsubr/callgsubr
	// (to limit accidentally or deliberately costly charstrings)
	CallCount int
	Width     *float64 // the glyph width, optional
	HStemHint [][]float64
	VStemHint [][]float64
	// Active hints
	// priority list of counters
	Stack       []float64
	ReturnStack []PC
	// seed
}

const TerpLimitNesting = 10

// A model for the program counter.
type PC struct {
	// the remaining string to process
	bs []byte
	// name of subroutine
	In string
}

// The bias for callsubr and callgsubr is computed (once hopefully)
// and stored in the terp.
func (terp *Terp) FixBias() {
	// Bias. According to Section 4.7 of [Type2Spec], quoted here:
	//   If the number of subrs (gsubrs) is less than 1240,
	//   the bias is 107. Otherwise if it is less than 33900,
	//   it is 1131; otherwise it is 32768.
	toBias := func(n int) int {
		if n < 1240 {
			return 107
		} else if n < 33900 {
			return 1131
		}
		return 32768
	}

	terp.BiasSubr = toBias(len(terp.Subr))
	terp.BiasGsubr = toBias(len(terp.Gsubr))
}

// Fetch the code (and a name) for the (global?) subr.
// The value v should be the _biased_ number, as it appears in
// the charstring byte stream.
// The returned name incorporates the _unbiased_ number,
// which will be used in the Subr/Gsubr section of the .ttx file.
func (terp *Terp) FetchSubr(global bool, v float64) PC {
	bias := terp.BiasSubr
	name := "subr"
	if global {
		bias = terp.BiasGsubr
		name = "gsubr"
	}

	// Spec authors seem to have forgotten that the input value
	// could be non-integer.
	if math.Trunc(v) != v {
		log.Fatalf("Fraction %v used for call%s %v",
			v, name, terp)
	}

	// Restore unbiased index, and fetch.
	i := int(v) + bias
	name = fmt.Sprintf("%s#%d", name, i)
	if global {
		return PC{terp.Gsubr[i], name}
	}
	return PC{terp.Subr[i], name}
}

// How many bytes follow a hintmask operator?
func (terp *Terp) MaskBytes() int {
	n := len(terp.HStemHint) + len(terp.VStemHint)
	return (n + 7) / 8
}

// Set (optioonal) Width, based on whether the stack length is odd or even.
func (terp *Terp) SetWidth() {
	if len(terp.Stack)&1 == 0 {
		return
	}
	if terp.Width != nil {
		log.Print("non-even stack depth for stem/mask/moveto/endchar operator is not permitted")
	}
	w := terp.Stack[0]
	terp.Stack = terp.Stack[1:]
	terp.Width = &w
}

// Set (optioonal) Width, based on whether the stack has length 2.
// Same as SetWidth, but for hmoveto and vmoveto
func (terp *Terp) SetWidth2() {
	if len(terp.Stack) != 2 {
		return
	}
	if terp.Width != nil {
		log.Print("Spurious stack depth for h/vmoveto operator")
	}
	w := terp.Stack[0]
	terp.Stack = terp.Stack[1:]
	terp.Width = &w
}

type TerpAction func(terp *Terp, code string)

func ActionPrint(terp *Terp, code string) {
	if len(terp.Stack) == 0 {
		log.Print(code)
	} else {
		log.Print(strings.Trim(fmt.Sprint(terp.Stack), "[]"),
			" ",
			code)
	}
}

// Return a TerpAction and an Outline.
// Executing the action (by passing it to
// InterpretCharstring) has the effect of
// accumulating contours into the returned outline.
func MakeActionOutline() (TerpAction, *coff.Outline) {
	point := coff.Point{}     // current point
	var contour *coff.Contour // nil when closed, non-nil when open
	outline := &coff.Outline{}

	open := func() {
		// Open the contour, if needed.
		if contour != nil {
			return
		}
		contour = &coff.Contour{
			P: []coff.ContourPoint{
				{point.X, point.Y, true}},
		}
	}
	close := func() {
		// Close the contour, if needed.
		if contour == nil {
			return
		}
		outline.Contours = append(outline.Contours,
			*contour)
		contour = nil
	}

	// The general case of adding a (cubic Bézier) curve to the
	// current contour.
	addRCurve := func(dxa, dya, dxb, dyb, dxc, dyc float64) {
		// Starting point, if needed.
		open()
		// Convert to absolute
		ax := point.X + dxa
		ay := point.Y + dya
		bx := ax + dxb
		by := ay + dyb
		cx := bx + dxc
		cy := by + dyc
		contour.P = append(contour.P,
			[]coff.ContourPoint{
				{ax, ay, false},
				{bx, by, false},
				{cx, cy, true}}...)
		point = coff.Point{cx, cy}
	}
	// Add a line.
	addRLine := func(dxa, dya float64) {
		open()
		// Convert to absolute
		ax := point.X + dxa
		ay := point.Y + dya
		contour.P = append(contour.P,
			coff.ContourPoint{ax, ay, true})
		point = coff.Point{ax, ay}
	}

	// Stack diagrams, for example
	//     |- dx1? {dya dxb dyb dyc}+ vvcurveto (26) |-
	// are provided from [Type2Spec], and are copied as-is.
	// The notation is not always consistent.
	return func(terp *Terp, code string) {
		// aLine - shared for hlineto and vlineto
		// vert parameter is true when initial motion is
		// vertical, and subsequently controls whether
		// next line is vertical or horizontal.
		aLine := func(vert bool) {
			// |- dx1 {dya dxb}* hlineto (6) |-
			// |- {dxa dyb}+ hlineto (6) |-
			// same stack pattern for vlineto, but swapped.

			for len(terp.Stack) > 0 {
				ax := terp.Stack[0]
				ay := 0.0
				if vert {
					ax, ay = ay, ax
				}
				addRLine(ax, ay)
				terp.Stack = terp.Stack[1:]
				vert = !vert
			}
			return
		}

		// aCurve - shared for hvcurveto and vhcurveto
		// as in aLine the vert parameter
		// controls whether the next motion is
		// vertical or not, and flips each go
		// around the loop.
		aCurve := func(vert bool) {
			// |- dy1 dx2 dy2 dx3 {dxa dxb dyb dyc dyd dxe dye dxf}* dyf? vhcurveto (30) |-
			// |- {dya dxb dyb dxc dxd dxe dye dyf}+ dxf? vhcurveto (30) |-
			// One way to think of this is that
			// every 4 arguments defines a curve
			// that starts vertical (or horizontal)
			// and ends horizontal (or vertical).
			// V to H alternates with H to V.
			// With one additional wrinkle:
			// if there are exactly 5 arguments
			// remaining then the last curve has
			// both X and Y specified for its final
			// point, and need not be horizontal (or
			// vertical).

			if len(terp.Stack)%4 > 1 {
				log.Printf("must have stack depth a multiple of 4 (is %v)\n",
					len(terp.Stack))
			}

			for len(terp.Stack) >= 4 {
				dxa := 0.0
				dya := terp.Stack[0]
				dxb := terp.Stack[1]
				dyb := terp.Stack[2]
				dxc := terp.Stack[3]
				dyc := 0.0
				if len(terp.Stack) == 5 {
					dyc = terp.Stack[4]
				}
				if !vert {
					dxa, dya = dya, dxa
					dxc, dyc = dyc, dxc
				}
				addRCurve(dxa, dya, dxb, dyb, dxc, dyc)
				terp.Stack = terp.Stack[4:]
				vert = !vert
			}
			terp.Stack = []float64{}
			return
		}

		switch code {
		// Apart from endchar (immediately below),
		// the other operators are given in the same
		// order as Section 4 of [Type2Spec].
		case "endchar":
			close()
			terp.SetWidth()
			return

		case "rmoveto":
			// Close contour
			close()
			// Optional width argument.
			terp.SetWidth()
			if len(terp.Stack) != 2 {
				log.Printf("rmoveto must have stack depth of 2 (is %v)\n",
					len(terp.Stack))
				goto clear
			}

			point.X += terp.Stack[0]
			point.Y += terp.Stack[1]
		clear:
			terp.Stack = []float64{}
			return
		case "hmoveto":
			// Close contour
			close()
			// Optional width argument.
			terp.SetWidth2()
			if len(terp.Stack) != 1 {
				log.Printf("hmoveto must have stack depth of 1 (is %v)\n",
					len(terp.Stack))
				goto hclear
			}
			point.X += terp.Stack[0]
		hclear:
			terp.Stack = []float64{}
			return
		case "vmoveto":
			// Close contour
			close()
			// Optional width argument.
			terp.SetWidth2()
			if len(terp.Stack) != 1 {
				log.Printf("vmoveto must have stack depth of 1 (is %v)\n",
					len(terp.Stack))
				goto vclear
			}
			point.Y += terp.Stack[0]
		vclear:
			terp.Stack = []float64{}
			return

		case "rlineto":
			// |- {dxa dya}+ rlineto (5) |-
			for len(terp.Stack) >= 2 {
				ax := terp.Stack[0]
				ay := terp.Stack[1]
				addRLine(ax, ay)
				terp.Stack = terp.Stack[2:]
			}
			terp.Stack = []float64{}
			return

		case "hlineto":
			aLine(false)
			return
		case "vlineto":
			aLine(true)
			return

		case "rrcurveto", "rcurveline":
			// The implementations of these
			// operators can be combined.
			// rrcurveto requires 6n arguments, and
			// rcurveline requires 6n+2 arguments.
			// The 6-tuples are processed the same
			// way in each case (each one becomes a
			// single curve).
			// And we can implement rcurveline by
			// adding a line if and only if there
			// are 2 arguments remaining.
			// |- {dxa dya dxb dyb dxc dyc}+ rrcurveto (8) |-
			// |- {dxa dya dxb dyb dxc dyc}+ dxd dyd rcurveline (24) |-
			// the curve part
			for len(terp.Stack) >= 6 {
				dxa := terp.Stack[0]
				dya := terp.Stack[1]
				dxb := terp.Stack[2]
				dyb := terp.Stack[3]
				dxc := terp.Stack[4]
				dyc := terp.Stack[5]
				addRCurve(dxa, dya, dxb, dyb, dxc, dyc)
				terp.Stack = terp.Stack[6:]
			}
			// the line part of rcurveline
			if len(terp.Stack) == 2 {
				dxa := terp.Stack[0]
				dya := terp.Stack[1]
				addRLine(dxa, dya)
				terp.Stack = terp.Stack[2:]
			}

			if len(terp.Stack) != 0 {
				log.Printf("Spurious extra arguments in %v\n", code)
			}

			terp.Stack = []float64{}
			return
		case "hhcurveto":
			// |- dy1? {dxa dxb dyb dxc}+ hhcurveto (27) |-
			// An odd-length stack implies the first curve has
			// its dya argument specified (and
			// therefore will not in general begin horizontal).
			// But all the curves end horizontal.
			dya := 0.0
			if len(terp.Stack)&1 == 1 {
				dya = terp.Stack[0]
				terp.Stack = terp.Stack[1:]
			}
			if len(terp.Stack)%4 != 0 {
				log.Printf("hhcurveto must have stack depth a multiple of 4 (is %v)\n", len(terp.Stack))
			}
			for len(terp.Stack) >= 4 {
				dxa := terp.Stack[0]
				dxb := terp.Stack[1]
				dyb := terp.Stack[2]
				dxc := terp.Stack[3]
				addRCurve(dxa, dya, dxb, dyb, dxc, 0)
				dya = 0
				terp.Stack = terp.Stack[4:]
			}
			terp.Stack = []float64{}
			return
		case "hvcurveto":
			aCurve(false)
			return
			// case rcurveline: see rrcurveto
		case "rlinecurve":
			for len(terp.Stack) >= 8 {
				dxa := terp.Stack[0]
				dxb := terp.Stack[1]
				addRLine(dxa, dxb)
				terp.Stack = terp.Stack[2:]
			}
			dxa := terp.Stack[0]
			dya := terp.Stack[1]
			dxb := terp.Stack[2]
			dyb := terp.Stack[3]
			dxc := terp.Stack[4]
			dyc := terp.Stack[5]
			addRCurve(dxa, dya, dxb, dyb, dxc, dyc)
			terp.Stack = []float64{}
			return
		case "vhcurveto":
			aCurve(true)
			return
		case "vvcurveto":
			// |- dx1? {dya dxb dyb dyc}+ vvcurveto (26) |-
			// (Like hhcurveto, but vertical)
			// An odd-length stack implies the first curve has
			// its dxa argument specified (and
			// therefore will not in general begin vertical).
			// But all the curves end vertical.
			dxa := 0.0
			if len(terp.Stack)&1 == 1 {
				dxa = terp.Stack[0]
				terp.Stack = terp.Stack[1:]
			}
			if len(terp.Stack)%4 != 0 {
				log.Printf("vvcurveto must have stack depth a multiple of 4 (is %v)\n", len(terp.Stack))
			}
			for len(terp.Stack) >= 4 {
				dya := terp.Stack[0]
				dxb := terp.Stack[1]
				dyb := terp.Stack[2]
				dyc := terp.Stack[3]
				addRCurve(dxa, dya, dxb, dyb, 0, dyc)
				dxa = 0
				terp.Stack = terp.Stack[4:]
			}
			terp.Stack = []float64{}
			return

			// :todo: flex

		case "hstem", "hstemhm", "vstem", "vstemhm":
			// No action needed here
			// :todo: cntrmask
		default:
			if (code + "XXXXXXXX")[4:8] == "mask" {
				break
			}
			log.Printf("UNIMPLEMENTED Charstring operator %v\n", code)
		}

	}, outline
}

// Interprets (Type 2) charstring.
// This interpretation provides a skeleton,
// the actual drawing (or printing or flattening) is provided
// by `action`.
//   - root is the starting byte stream (a glyph).
//   - subr and gsubr are slices of the byte streams used
//     for callsubr and callgsubr.
//
// The contents of subr and gsubr will not be modified by this function,
// but also should not be modified by other code during this call.
// [Type2Spec] Appendix A most useful.
func InterpretCharstring(
	root []byte, subr, gsubr [][]byte, action TerpAction,
) {
	// Processing works like a stack machine.
	// The stack is a stack of float64 numbers.
	// The byte stream is processed byte-by-byte, with the next
	// byte determing whether we have an operand,
	// which is pushed onto the stack, or an operator.
	// The operators are roughly divided into
	// geometry/drawing (like `rlineto`) and arithmetic
	// (like 'add').
	// Generally the drawing operators clear the stack,
	// and the arithmetic operators do not.
	// Note that the arithmetic operators are unimplemented,
	// and its also worth noting that in the later `CFF2`
	// tables in OTF they are removed from the spec.

	terp := &Terp{}
	terp.Subr = subr
	terp.Gsubr = gsubr
	terp.FixBias()

	terp.bs = root
	terp.In = "Root"

	vstem := func() {
		terp.SetWidth()
		// Note: steps in pairs
		for i := 0; i < len(terp.Stack); i += 2 {
			terp.VStemHint = append(terp.VStemHint,
				[]float64{terp.Stack[i], terp.Stack[i+1]})
		}
	}
	// Implicit vmstem
	// :todo: the condition here is far from ideal.
	// The wording in the spec is:
	//  "If hstem and vstem hints are both
	//   declared at the beginning of a
	//   charstring, and this sequence is followed
	//   directly by the hintmask or cntrmask
	//   operators, the vstem hint operator need
	//   not be included"
	// What if `hstemhm` is used but with 0 hints?
	// What if `vstemhm` is used multiple times?
	// The stack diagram for `hintmask` and `cntrmask`
	// shows an empty stack before and after.
	// Therefore i choose to implement the stack
	// effect of this operator as for `vstemhm`.
	// Although this would be an error (in
	// the Type 2 Charstring) for this to
	// occur after a non-stem operator.
	impvstem := vstem

	// Process operator
	pop := func(code string) {
		switch code {
		case "return":
			d := len(terp.ReturnStack)
			if d == 0 {
				log.Fatal("`return` encountered, but not in subroutine", terp)
			}
			target := terp.ReturnStack[d-1]
			terp.ReturnStack = terp.ReturnStack[:d-1]
			terp.PC = target
			return
		case "callsubr", "callgsubr":
			r := len(terp.ReturnStack)
			if r >= TerpLimitNesting {
				log.Fatal("`callsubr` nesting too deep", terp)
			}
			// get (biased) subr number and then its code
			d := len(terp.Stack)
			v := terp.Stack[d-1]
			terp.Stack = terp.Stack[:d-1]
			target := terp.FetchSubr(code == "callgsubr", v)
			// push current PC and switch
			terp.ReturnStack = append(terp.ReturnStack, terp.PC)
			terp.PC = target
			return
		}

		action(terp, code)

		switch code {
		case "hstem", "hstemhm":
			terp.SetWidth()
			// Note: steps in pairs
			for i := 0; i < len(terp.Stack); i += 2 {
				terp.HStemHint = append(terp.HStemHint,
					[]float64{terp.Stack[i], terp.Stack[i+1]})
			}
		case "vstem", "vstemhm":
			vstem()
		}

		terp.Stack = []float64{}
	}
	// Process operator after consuming one byte of byte stream.
	pop1 := func(code string) {
		terp.bs = terp.bs[1:]
		pop(code)
	}
	// Push single value.
	push := func(v float64) {
		terp.Stack = append(terp.Stack, v)
	}

	for len(terp.bs) > 0 {
		b := terp.bs[0]
		switch {
		// [Type2Spec] Appendix A—Type 2 Charstring Command Codes
		// 0 to 31 specify operators.
		// 12 and 28 represent extended codes and 2 byte integers.
		case 12 == b:
			// extended codes
			terp.bs = terp.bs[1:]
			s := terp.bs[0]
			switch s {
			case 3:
				pop1("and")
			case 4:
				pop1("or")
			case 5:
				pop1("not")

			case 9:
				pop1("abs")
			case 10:
				pop1("add")
			case 11:
				pop1("sub")
			case 12:
				pop1("div")

			case 14:
				pop1("neg")
			case 15:
				pop1("eq")

			case 18:
				pop1("drop")

			case 20:
				pop1("put")
			case 21:
				pop1("get")
			case 22:
				pop1("ifelse")
			case 23:
				pop1("random")
			case 24:
				pop1("mul")

			case 26:
				pop1("sqrt")
			case 27:
				pop1("dup")
			case 28:
				pop1("exch")
			case 29:
				pop1("index")
			case 30:
				pop1("roll")

			case 34:
				pop1("hflex")
			case 35:
				pop1("flex")
			case 36:
				pop1("hflex1")
			case 37:
				pop1("flex1")

			default:
				code := fmt.Sprintf("Unknown 12 code %d", s)
				pop1(code)
			}
		case 28 == b:
			// 2 byte int
			v := int16(binary.BigEndian.Uint16(terp.bs[1:3]))
			push(float64(v))
			terp.bs = terp.bs[3:]
		case 0 <= b && b <= 31:
			code := []string{"-Reserved-",
				"hstem",
				"-Reserved-",
				"vstem",
				"vmoveto",
				"rlineto",
				"hlineto",
				"vlineto",

				"rrcurveto",
				"-Reserved-",
				"callsubr",
				"return",
				"escape", // see case 12, above
				"-Reserved-",
				"endchar",
				"-Reserved-",

				"-Reserved-",
				"-Reserved-",
				"hstemhm",
				"hintmask",
				"cntrmask",
				"rmoveto",
				"hmoveto",
				"vstemhm",

				"rcurveline",
				"rlinecurve",
				"vvcurveto",
				"hhcurveto",
				"shortint", // see case 28, above
				"callgsubr",
				"vhcurveto",
				"hvcurveto",
			}[b]
			if code == "hintmask" || code == "cntrmask" {
				// Implicit vstemhm
				impvstem()
				terp.bs = terp.bs[1:]
				e := terp.MaskBytes()
				bits := terp.bs[:e]
				terp.bs = terp.bs[e:]

				if e > 0 {
					code += " "
				}

				nbits := len(terp.HStemHint) + len(terp.VStemHint)

				for i := 0; i < nbits; i += 1 {
					index := i / 8
					shift := 7 - (i % 8)
					bit := (bits[index] >> shift) & 1
					code += fmt.Sprint(bit)
				}
				pop(code)
			} else {
				pop1(code)
			}

			// operands, see Section 4 - DICT Data.
			// Same as Dict encoding
		case 32 <= b && b <= 246:
			v := int(b) - 139
			push(float64(v))
			terp.bs = terp.bs[1:]
			break
		case 247 <= b && b <= 250:
			v := (int(b)-247)*256 + int(terp.bs[1]) + 108
			push(float64(v))
			terp.bs = terp.bs[2:]
			break
		case 251 <= b && b <= 254:
			v := (251-int(b))*256 - int(terp.bs[1]) - 108
			push(float64(v))
			terp.bs = terp.bs[2:]
			break
		case 255 == b:
			// 4 byte 16.16 Fixed
			v := int32(binary.BigEndian.Uint32(terp.bs[1:5]))
			push(float64(v) / 65536)
			terp.bs = terp.bs[5:]
		default:
			log.Fatal("Unknown Charstring Command code ",
				b, terp)
		}
	}
	if len(terp.Stack) > 0 {
		pop("Missing Operator")
	}
}
