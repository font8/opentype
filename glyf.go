package opentype

// Mostly glyf to Outline stuff.

import (
	"bytes"
	"encoding/binary"
	"gitlab.com/font8/coff"
	"io"
	"log"
)

type TTGlyph struct {
	font  *T
	index int
}

func (g *TTGlyph) ToOutline() *coff.Outline {
	blocks, err := g.font.GlyphDataBlocks()
	if err != nil {
		log.Fatal(err)
	}
	bs := blocks[g.index]
	o := GlyfBytesToOutline(bs)
	return o
}

// Shifts that select X-axis or Y-axis in ReadCoord()
const XS = 0
const YS = 1

// Convert byte-level TrueType representation to Outline.
func GlyfBytesToOutline(block []byte) *coff.Outline {
	// Refer to OpenType spec for binary description of glyf table.
	// https://docs.microsoft.com/en-us/typography/opentype/spec/glyf

	r := bytes.NewBuffer(block)
	header := GlyfHeaderRecord{}
	binary.Read(r, binary.BigEndian, &header)

	if header.NumberOfContours < 0 {
		return GlyfBytesToCompositeOutline(r, header)
	}
	return GlyfBytesToSimpleOutline(r, header)
}

func GlyfBytesToSimpleOutline(
	r io.Reader, header GlyfHeaderRecord,
) *coff.Outline {
	EndPtsOfContours := make([]uint16, header.NumberOfContours)
	binary.Read(r, binary.BigEndian, EndPtsOfContours)

	var InstructionLength uint16
	binary.Read(r, binary.BigEndian, &InstructionLength)

	Instructions := make([]byte, InstructionLength)
	binary.Read(r, binary.BigEndian, &Instructions)

	// index of last point in glyph
	lastPointIndex := -1
	if header.NumberOfContours > 0 {
		lastPointIndex = int(EndPtsOfContours[header.NumberOfContours-1])
	}

	flags := GlyfExpandFlags(r, lastPointIndex+1)
	xs := GlyfReadCoord(r, flags, XS)
	ys := GlyfReadCoord(r, flags, YS)

	contours := AsContours(EndPtsOfContours, flags, xs, ys)
	contours = coff.ExpandImplicitTTContours(contours)
	return &coff.Outline{Contours: contours}
}

// Refer to spec: https://docs.microsoft.com/en-us/typography/opentype/spec/glyf#composite-glyph-description
func GlyfBytesToCompositeOutline(r io.Reader, header GlyfHeaderRecord,
) *coff.Outline {
	o := coff.Outline{}
	for {
		component := coff.Component{}
		var flags, glyphIndex uint16
		var arg1, arg2 int

		binary.Read(r, binary.BigEndian, &flags)
		binary.Read(r, binary.BigEndian, &glyphIndex)
		component.GlyphIndex = int(glyphIndex)

		// ARG_1_AND_2_ARE_WORDS
		if 0 != flags&1 {
			arg := make([]int16, 2)
			binary.Read(r, binary.BigEndian, arg)
			arg1 = int(arg[0])
			arg2 = int(arg[1])
		} else {
			arg := make([]int8, 2)
			binary.Read(r, binary.BigEndian, arg)
			arg1 = int(arg[0])
			arg2 = int(arg[1])
		}

		var scale []int16

		// WE_HAVE_A_SCALE
		if 0 != flags&8 {
			scale = make([]int16, 1)
		}
		// WE_HAVE_AN_X_AND_Y_SCALE
		if 0 != flags&0x40 {
			scale = make([]int16, 2)
		}
		// WE_HAVE_A_TWO_BY_TWO
		if 0 != flags&0x80 {
			scale = make([]int16, 4)
		}
		binary.Read(r, binary.BigEndian, scale)

		component.SetMatrix(flags, arg1, arg2, scale)

		o.Components = append(o.Components, component)

		// MORE_COMPONENTS
		if flags&0x20 == 0 {
			break
		}
	}
	return &o
}

// Read the glyf flags from r into fully expanded form
func GlyfExpandFlags(r io.Reader, l int) []byte {
	b := make([]byte, 1)
	flags := []byte{}
	for len(flags) < l {
		_, err := io.ReadFull(r, b)
		if err != nil {
			return flags
		}

		flag := b[0]
		flags = append(flags, flag)

		// REPEAT_FLAG
		if flag&0x08 != 0 {
			io.ReadFull(r, b)
			n := b[0]
			for i := byte(0); i < n; i++ {
				flags = append(flags, flag)
			}
		}
	}
	return flags
}

// From the reader r, read out a sequence of coordinates
// (the returned value), under the control of flags.
// flags is used to determine the number of bytes to read for each
// coordinate (0, 1, or 2).
// shift (which should be XS or YS), is used to pick out
// the flags bits for the appropriate axis.
func GlyfReadCoord(r io.Reader, flags []byte, shift int) []int16 {
	if shift < 0 || 1 < shift {
		log.Fatal("shift out of range", shift, flags)
	}
	coords := []int16{}
	var SHORT = byte(0x02) << shift
	var POSITIVE = byte(0x10) << shift
	var SAME = byte(0x10) << shift

	for _, flag := range flags {
		if flag&SHORT != 0 {
			// 9 bits, sign (from POSITIVE bit) and magnitude byte.
			var b byte
			binary.Read(r, binary.BigEndian, &b)
			if flag&POSITIVE == 0 {
				coords = append(coords, -int16(b))
			} else {
				coords = append(coords, int16(b))
			}
		} else if flag&SAME != 0 {
			// 0 bits
			coords = append(coords, 0)
		} else {
			// 16 bits
			var s int16
			binary.Read(r, binary.BigEndian, &s)
			coords = append(coords, s)
		}
	}
	return coords
}

// Convert to sequence of Contour
func AsContours(ends []uint16, flags []byte, xs, ys []int16) []coff.Contour {
	cs := []coff.Contour{}

	if len(flags) != len(xs) ||
		len(flags) != len(ys) {
		log.Fatal("Coordinate arrays should have equal lengths",
			flags, xs, ys)
	}

	if len(flags) == 0 {
		return cs
	}

	if len(ends) < 1 {
		log.Fatal("Empty contour array")
	}

	lastEnd := ends[len(ends)-1]
	if int(lastEnd) != len(flags)-1 {
		log.Fatal("Final contour is unterminated",
			ends, flags)
	}

	// Convert to absolute.
	baseline := float64(0)

	absx := make([]float64, len(xs))
	absy := make([]float64, len(ys))
	absx[0] = float64(xs[0])
	absy[0] = baseline + float64(ys[0])
	for i := 1; i < len(flags); i++ {
		absx[i] = absx[i-1] + float64(xs[i])
		absy[i] = absy[i-1] + float64(ys[i])
	}

	// Number of contours completed.
	cIndex := 0

	contour := coff.Contour{}

	for i, flag := range flags {
		// ON_CURVE_POINT
		on_curve := flag&0x1 != 0
		point := coff.ContourPoint{absx[i], absy[i], on_curve}
		contour.P = append(contour.P, point)
		if int(ends[cIndex]) == i {
			cIndex++
			cs = append(cs, contour)
			contour = coff.Contour{}
		}
	}
	return cs
}
