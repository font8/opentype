package opentype

// name—Naming Table

import (
	"bytes"
	"encoding/binary"
	"encoding/csv"
	"fmt"
	"log"
	"strings"
	"unicode/utf16"
)

type NameTable struct {
	NameHeaderRecord
	Lrec []LangTagRecord
	Nrec []NameRecord
	B    []byte
}

type NameHeaderRecord struct {
	Format       uint16
	Count        uint16
	StringOffset uint16
}

// https://docs.microsoft.com/en-gb/typography/opentype/spec/name#name-records
type NameRecord struct {
	PlatformID, EncodingID, LanguageID, NameID uint16
	Length, Offset                             uint16
}

// Only used in Format 1
// https://docs.microsoft.com/en-gb/typography/opentype/spec/name#naming-table-format-1
type LangTagRecord struct {
	Length, Offset uint16
}

// Decoded and modelled names.
// Because of platform and encoding issues, it's possible that
// not all NameRecord objects can be successfully decoded.
type Name struct {
	NameRecord
	PlatEnc string // Platform and Encoding as single string
	Lang    string
	Value   string
}

func NewName(table Table) *NameTable {
	r := bytes.NewReader(table.B)

	h := NameHeaderRecord{}

	binary.Read(r, binary.BigEndian, &h)
	ns := make([]NameRecord, h.Count)
	binary.Read(r, binary.BigEndian, ns)

	var langTagCount uint16
	var lrecs []LangTagRecord

	switch h.Format {
	case 0:
		// do nothing
	case 1:
		binary.Read(r, binary.BigEndian, &langTagCount)
		lrecs = make([]LangTagRecord, langTagCount)
		binary.Read(r, binary.BigEndian, lrecs)
	default:
		log.Fatalf("unknown name table format %d", h.Format)
	}

	return &NameTable{h, lrecs, ns, table.B}
}

// Return the "Full Name" of the font,
// derived from the OpenType name table.
// This function uses the name record with Name ID 4;
// the OpenType spec describes Name ID 4 as "Full font name".
//
// This function searches only the Windows platform records,
// and returns the first one from the following sequence:
// - the first English-language record;
// - the first record;
// - the empty string "" (there are no Windows platform records)
//
// Notes on Platform/Encoding/Language.
// Each record has a Platform/Encoding/Language triple;
// a given ID (such as 4) may have several records.
// The intention behind Platform is that one code is used for
// Macintosh and one code is used for Windows.
//
// In practice in 2022, modern fonts supply Windows platform records,
// regardless of the intended target;
// all rendering systems can interpret Windows platform records.
//
// Most fonts also supply Macintosh platform records but usage varies:
// - duplicate set (for example Ubuntu Mono)
// - none, only Windows platform (for example Zilla Slab, Roboto Mono)
// - minimal set (historical MS system fonts)
// There is also a _Unicode_ platform, presumably intended for
// platforms such as Unix and Plan 9, very few fonts seems to use it
// (some MS fonts supplied by Monotype such as Arial and Times New Roman).
// The Unicode platform seems largely irrelevant in 2022.
func (nt *NameTable) FullName() string {
	platform := 3
	id := 4 // https://docs.microsoft.com/en-us/typography/opentype/spec/name#name-records

	names := []*Name{}
	for _, record := range nt.Nrec {
		if int(record.NameID) != id {
			continue
		}
		if int(record.PlatformID) != platform {
			continue
		}

		name := nt.DecodeName(&record)
		names = append(names, name)
	}

	if len(names) == 0 {
		return ""
	}
	for _, name := range names {
		if strings.HasPrefix(name.Lang, "en-") {
			return name.Value
		}
	}
	return names[0].Value
}

// Show the name record in printable form.
func (n *NameTable) RecordToString(record *NameRecord) string {
	row := make([]string, 5)
	row[0] = "name"

	name := n.DecodeName(record)

	if name.PlatEnc == "" {
		return ""
	}
	row[1] = name.PlatEnc

	if name.Lang != "" {
		row[2] = name.Lang
	} else {
		row[2] = fmt.Sprintf("%#04x", name.LanguageID)
	}
	row[3] = fmt.Sprintf("%d", record.NameID)
	row[4] = name.Value

	buf := &bytes.Buffer{}
	csvw := csv.NewWriter(buf)
	csvw.Write(row)
	csvw.Flush()

	return strings.TrimSpace(string(buf.Bytes()))
}

func (n *NameTable) DecodeName(record *NameRecord) *Name {
	name := &Name{NameRecord: *record}

	platenc := (uint32(record.PlatformID) << 16) |
		uint32(record.EncodingID)
	switch platenc {
	case 0x30001:
		name.PlatEnc = "Win/BMP"
	case 0x00003:
		name.PlatEnc = "Unicode/BMP"
	case 0x00004:
		name.PlatEnc = "Unicode/BMP+"
	}

	switch record.LanguageID {
	case 0x409:
		name.Lang = "en-US"
	case 0x809:
		name.Lang = "en-GB"
	}

	if name.PlatEnc != "" {
		bs := n.B[n.StringOffset:]
		n := bs[record.Offset : record.Offset+record.Length]
		name.Value = UTF16DecodeBE(n)
	}
	return name
}

func UTF16DecodeBE(bs []byte) string {
	runes := make([]uint16, len(bs)/2)
	binary.Read(bytes.NewReader(bs), binary.BigEndian, runes)
	return string(utf16.Decode(runes))
}
